import { ENamPage } from './app.po';

describe('e-nam App', () => {
  let page: ENamPage;

  beforeEach(() => {
    page = new ENamPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
