// export const environment = {
//   production: true,
//   testUrl:"http://staging.techl33t.com:8181/enam/api/view/",
//   authUrl:"http://staging.techl33t.com:8181/enam/api/view/",
//   loginUrl:"http://staging.techl33t.com:8181/enam/api/login",
//   logoutUrl:"http://staging.techl33t.com:8181/enam/api/logout",
//   bidauthUrl:"http://staging.techl33t.com:8181/enam/api/view/newBidList/",
//   getpdf:"http://staging.techl33t.com:8181/enam/api/view/pdf",
//   addprefer:"http://staging.techl33t.com:8181/enam/api/view/onAddPrefferedCommodity",
//   bidnow:"http://staging.techl33t.com:8181/enam/api/view/bidNow",
//   eng:"http://staging.techl33t.com/enam/dist/",
//   hi:"http://staging.techl33t.com/enam/dist/hi",
//   gu:"http://staging.techl33t.com/enam/dist/gu",
//   te:"http://staging.techl33t.com/enam/dist/te"
// };
export const environment = {
  production: true,
  testUrl:"http://train.enam.gov.in/enam/api/view/",
  authUrl:"http://train.enam.gov.in/enam/api/view/",
  loginUrl:"http://train.enam.gov.in/enam/api/login",
  logoutUrl:"http://train.enam.gov.in/enam/api/logout",
  bidauthUrl:"http://train.enam.gov.in/enam/api/view/newBidList/",
  getpdf:"http://train.enam.gov.in/enam/api/view/pdf",
  addprefer:"http://train.enam.gov.in/enam/api/view/onAddPrefferedCommodity",
  bidnow:"http://train.enam.gov.in/enam/api/view/bidNow",
  eng:"http://train.enam.gov.in/dist/",
  hi:"http://train.enam.gov.in/dist/hi",
  gu:"http://train.enam.gov.in/dist/gu",
  te:"http://train.enam.gov.in/dist/te"
};
