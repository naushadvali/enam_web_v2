// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  testUrl:"http://staging.techl33t.com:8181/enam/api/view/",
  authUrl:"http://staging.techl33t.com:8181/enam/api/view/",
  loginUrl:"http://staging.techl33t.com:8181/enam/api/login",
  logoutUrl:"http://staging.techl33t.com:8181/enam/api/logout",
  bidauthUrl:"http://staging.techl33t.com:8181/enam/api/view/newBidList/",
  getpdf:"http://staging.techl33t.com:8181/enam/api/view/pdf",
  addprefer:"http://staging.techl33t.com:8181/enam/api/view/onAddPrefferedCommodity",
  bidnow:"http://staging.techl33t.com:8181/enam/api/view/bidNow",
  eng:"http://staging.techl33t.com/enam/dist/",
  hi:"http://staging.techl33t.com/enam/dist/hi",
  gu:"http://staging.techl33t.com/enam/dist/gu",
  te:"http://staging.techl33t.com/enam/dist/te"
};

// export const environment = {
//   production: false,
//   testUrl:"http://train.enam.gov.in/enam/api/view/",
//   authUrl:"http://train.enam.gov.in/enam/api/view/",
//   loginUrl:"http://train.enam.gov.in/enam/api/login",
//   logoutUrl:"http://train.enam.gov.in/enam/api/logout",
//   bidauthUrl:"http://train.enam.gov.in/enam/api/view/newBidList/",
//   getpdf:"http://train.enam.gov.in/enam/api/view/pdf",
//   addprefer:"http://train.enam.gov.in/enam/api/view/onAddPrefferedCommodity",
//   bidnow:"http://train.enam.gov.in/enam/api/view/bidNow",
//   eng:"http://train.enam.gov.in/dist/",
//   hi:"http://train.enam.gov.in/dist/hi",
//   gu:"http://train.enam.gov.in/dist/gu",
//   te:"http://train.enam.gov.in/dist/te"
// };

//Used in env-specific.json
// {
//   "testUrl":"http://train.enam.gov.in/enam/api/view/",
//   "authUrl":"http://train.enam.gov.in/enam/api/view/",
//   "loginUrl":"http://train.enam.gov.in/enam/api/login",
//   "logoutUrl":"http://train.enam.gov.in/enam/api/logout",
//   "bidauthUrl":"http://train.enam.gov.in/enam/api/view/newBidList/",
//   "bidauthUrlPagination":"http://train.enam.gov.in/enam/api/view/newBidListPagi/",
//   "getpdf":"http://train.enam.gov.in/enam/api/view/challanOnPendingInvoice/pdf",
//   "addprefer":"http://train.enam.gov.in/enam/api/view/onAddPrefferedCommodity",
//   "bidnow":"http://train.enam.gov.in/enam/api/view/bidNow",
//   "eng":"http://train.enam.gov.in/dist/",
//   "hi":"http://train.enam.gov.in/dist/hi",
//   "gu":"http://train.enam.gov.in/dist/gu",
//   "te":"http://train.enam.gov.in/dist/te",
//   "bn":"http://train.enam.gov.in/dist/bn",
//   "mr":"http://train.enam.gov.in/dist/mr"
// }

// {
//   "testUrl":"http://staging.techl33t.com:8181/enam/api/view/",
//   "authUrl":"http://staging.techl33t.com:8181/enam/api/view/",
//   "loginUrl":"http://staging.techl33t.com:8181/enam/api/login",
//   "logoutUrl":"http://staging.techl33t.com:8181/enam/api/logout",
//   "bidauthUrl":"http://staging.techl33t.com:8181/enam/api/view/newBidList/",
//   "getpdf":"http://staging.techl33t.com:8181/enam/api/view/pdf",
//   "addprefer":"http://staging.techl33t.com:8181/enam/api/view/onAddPrefferedCommodity",
// "bidauthUrlPagination":"http://staging.techl33t.com:8181/enam/api/view/newBidListPagi/",
//   "bidnow":"http://staging.techl33t.com:8181/enam/api/view/bidNow",
//   "eng":"http://staging.techl33t.com/enam/dist/",
//   "hi":"http://staging.techl33t.com/enam/dist/hi",
//   "gu":"http://staging.techl33t.com/enam/dist/gu",
//   "te":"http://staging.techl33t.com/enam/dist/te",
//   "bn":"http://staging.techl33t.com/enam/dist/bn",
//   "mr":"http://staging.techl33t.com/enam/dist/mr"
// }
// {
//   "testUrl":"http://staging.techl33t.com:8181/enam/api/view/",
//   "authUrl":"http://192.168.0.127:8080/enam/api/view/",
//   "loginUrl":"http://staging.techl33t.com:8181/enam/api/login",
//   "logoutUrl":"http://staging.techl33t.com:8181/enam/api/logout",
//   "bidauthUrl":"http://192.168.0.127:8080/enam/api/view/newBidList/",
//   "bidauthUrlPagination":"http://192.168.0.127:8080/enam/api/view/newBidListPagi/",
//   "getpdf":"http://staging.techl33t.com:8181/enam/api/view/challanOnPendingInvoice/pdf",
//   "addprefer":"http://staging.techl33t.com:8181/enam/api/view/onAddPrefferedCommodity",
//   "bidnow":"http://192.168.0.127:8080/enam/api/view/bidNow",
//   "eng":"http://staging.techl33t.com/dist/",
//   "hi":"http://staging.techl33t.com/dist/hi",
//   "gu":"http://staging.techl33t.com/dist/gu",
//   "te":"http://staging.techl33t.com/dist/te",
//   "bn":"http://staging.techl33t.com/dist/bn",
//   "mr":"http://staging.techl33t.com/dist/mr"
// }
