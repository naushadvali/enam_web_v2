import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  
  transform(list: any[], key: any): any {
    if (!list || key == "" || key == null) {
    return list;
    }
    return list.filter(function(item) {
    for (let property in item) {
    if (item[property] === null) {
    continue;
    }
    if (
    item[property]
    .toString()
    .toLowerCase()
    .includes(key.toLowerCase())
    ) {
    return true;
    }
    }
    return false;
    });
    }

}