import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogisticsservicesComponent } from './logisticsservices.component';

describe('LogisticsservicesComponent', () => {
  let component: LogisticsservicesComponent;
  let fixture: ComponentFixture<LogisticsservicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogisticsservicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogisticsservicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
