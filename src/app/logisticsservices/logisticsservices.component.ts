import { Component, OnInit } from '@angular/core';
import { LogisticsService } from '../services/logistics.service';
import { FilterPipe } from '../pipes/filter.pipe';
@Component({
  selector: 'app-logisticsservices',
  templateUrl: './logisticsservices.component.html',
  styleUrls: ['./logisticsservices.component.css']
})
export class LogisticsservicesComponent implements OnInit {

  starList: boolean[] = [true,true,true,true,true];
  rating:number;
  tableData:any [];
  searchText:any;

  constructor(private logistics:LogisticsService) { }

  setStar(data:number,obj:any){
      // this.rating=data+1;
      // for(var i=0;i<=4;i++){
      //   if(i<=data){  
      //     this.starList[i]=false;
      //   }
      //   else{
      //     this.starList[i]=true;
      //   }
      // }

      // console.log("selected",data);
      // console.log("object",obj);
      obj.rating=(data + 1).toString();
      // console.log("updated object",obj);
      this.logistics.getRating(obj).subscribe(res=>{
        // console.log("Rating Updated");
        this.loadData();
      });
  }

 loadData(){
   this.tableData = null;
   this.logistics.getData().subscribe(data=>{
    // console.log(data.listData);
    this.tableData = data.listData;
    //  console.log(this.tableData);
  });
 }

  ngOnInit() {
    this.loadData();
  }

  checkRating(item,data){

    if(item < parseInt(data.averageRating)){
      // console.log("item:",item,"data:",parseInt(data.averageRating));
      
      return true;
    }else{
      return false;
    }
  }

}
