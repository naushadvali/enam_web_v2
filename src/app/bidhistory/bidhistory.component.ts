import { Component,OnInit } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
@Component({

  selector: 'bidhistory',
  templateUrl: 'bidhistory.component.html',
  styleUrls: ['bidhistory.component.css']
})
export class BidhistoryComponent implements OnInit {
  data=[];
  bidFromDate:string;
  bidToDate:string;
  fromDate:string;
  toDate:string;
  adoumOprName:string;
  gmpmgProdName:string;
  iledLotCode:string;
  iledQTY:string;
  absBidDate:string;
  absBidRate:string;
  bidData=[];
  disableConfirm:boolean;
  bidhistoryError:any;
  public my_class4:any='overlay';


  getBidHistory(){

    if(this.bidFromDate==undefined || this.bidToDate==undefined){
      //this.disableConfirm=true;
      this.flashMessagesService.show('Dates are mandatory !', { cssClass: 'alert-danger', timeout: 3000 });
    }else{
      //this.disableConfirm=false;

          //console.log("bid to date",this.bidToDate);
          //console.log("bid from date",this.bidFromDate);
          const bidFromDate={
            day:this.bidFromDate["date"]["day"],
            month:this.bidFromDate["date"]["month"],
            year:this.bidFromDate["date"]["year"]
          }

          // const bidToDate={
          //   day:this.bidToDate["date"]["day"],
          //   month:this.bidToDate["date"]["month"],
          //   year:this.bidToDate["date"]["year"]
          // }
      this.fromDate=this.bidFromDate["date"]["year"]+"-"+this.bidFromDate["date"]["month"]+"-"+this.bidFromDate["date"]["day"];
      this.toDate=this.bidToDate["date"]["year"]+"-"+this.bidToDate["date"]["month"]+"-"+this.bidToDate["date"]["day"];
      //console.log("From Date:",this.fromDate);
      //console.log("From Date:",this.toDate);
      const dateJson={
        fromDate:this.fromDate,
        toDate:this.toDate

      }
          // this.bidhistoryError=sessionStorage.getItem('bidhistoryError');
          // if(this.bidhistoryError==0){
          //     this.flashMessagesService.show('Connection error', { cssClass: 'alert-danger', timeout: 3000 });
          //   }
          this.authservice.getBidHistory(dateJson).subscribe($data=>
            {
              //console.log("Reply from get bid history: ",$data);
              this.bidData=$data.listData;
              if(this.bidData.length==0){
                this.flashMessagesService.show('No records found ', { cssClass: 'alert-info', timeout: 3000 });
              }
          }
          , (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
        }
      }
        )
    }


  }

  errorPopupShow(){
    this.my_class4='overlay1';
  }

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd-mm-yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false
  };

  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private router:Router,
  ) {

  }
  ngOnInit(){
  }
}
