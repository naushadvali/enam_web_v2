import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
@Component({

  selector: 'bankdetails',
  templateUrl: 'bankdetails.component.html',
  styleUrls: ['bankdetails.component.css']
})
export class BankdetailsComponent {

  bankData:any=[];
  bankdetailsError:any;
  radioAgree:boolean;
  radioDisagree:boolean;
  public my_class4:any='overlay';
  public my_class5:any='overlay';

  adumOprID:any;
  adumOrgID:any;
  adumUserID:any;
  lcamAgenCompName:any;
  lcamAgenContNo:any;
  lcamAgenType:any;
  lcamDefunct:any;
  lcamFullName:any;
  lcamOprID:any;
  lcamOrgID:any;
  lcamTrnID:any;

  lcamAadharNo:any;
  lcamAgenEmailID:any;
  lcamBankAccount:any;
  lcamBankKycDate:any;
  lcamBankKycFlag:any;
  lcamBankName:any;
  lcamIfscCode:any;
  lcamPanNo:any;


  bankMessage:any;

  agrDisabled:boolean;
  disAgrDisabled:boolean;

  constructor(private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private router:Router,)
    {
      this.authservice.getBankDetails().subscribe
      (
        $data=>{
          this.bankData=$data.data;
          this.adumOprID=this.bankData.adumOprID;
          this.adumOrgID=this.bankData.adumOrgID;
          this.adumUserID=this.bankData.adumUserID;
          this.lcamAgenCompName=this.bankData.lcamAgenCompName;
          this.lcamAgenContNo=this.bankData.lcamAgenContNo;
          this.lcamAgenType=this.bankData.lcamAgenType;
          this.lcamDefunct=this.bankData.lcamDefunct;
          this.lcamFullName=this.bankData.lcamFullName;
          this.lcamOprID=this.bankData.lcamOprID;
          this.lcamOrgID=this.bankData.lcamOrgID;
          this.lcamTrnID=this.bankData.lcamTrnID;
          this.lcamAadharNo=this.bankData.lcamAadharNo;
          this.lcamAgenEmailID=this.bankData.lcamAgenEmailID;
          this.lcamBankAccount=this.bankData.lcamBankAccount;
          this.lcamBankKycDate=this.bankData.lcamBankKycDate;
          this.lcamBankKycFlag=this.bankData.lcamBankKycFlag;
          this.lcamBankName=this.bankData.lcamBankName;
          this.lcamIfscCode=this.bankData.lcamIfscCode;
          this.lcamPanNo=this.bankData.lcamPanNo;
          //console.log("lcamTrnID: ",this.lcamTrnID);
          if(this.lcamBankKycFlag=='Y'){
            this.radioAgree=true;
            this.radioDisagree=false;
          }else if(this.lcamBankKycFlag=='N'){
            this.radioDisagree=true;
            this.radioAgree=false;
          }
        }
        , (err) => {
          if (err === 'Unauthorized')
          {
            this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
          }
        }
      );
    this.agrDisabled =false;
    this.disAgrDisabled =false;

    }

    errorPopupShow(){
      this.my_class4='overlay1';
    }

    radioCheck(radioValue){



        const fullBankDetails={
         adumOprID:this.adumOprID,
         adumOrgID: this.adumOrgID,
         adumUserID: this.adumUserID,
         lcamAadharNo: this.lcamAadharNo,
         lcamAgenCompName:this.lcamAgenCompName,
         lcamAgenContNo: this.lcamAgenContNo,
         lcamAgenEmailID:this.lcamAgenEmailID,
         lcamAgenType: this.lcamAgenType,
         lcamBankAccount:this.lcamBankAccount,
         lcamBankKycDate: this.lcamBankKycDate,
         lcamBankKycFlag:this.lcamBankKycFlag,
         lcamBankName: this.lcamBankName,
         lcamDefunct: this.lcamDefunct,
         lcamFullName: this.lcamFullName,
         lcamIfscCode: this.lcamIfscCode,
         lcamOprID: this.lcamOprID,
         lcamOrgID:this.lcamOrgID,
         lcamPanNo: this.lcamPanNo,
         lcamTrnID: this.lcamTrnID,
        }




        this.authservice.sendBankAgrDgr(fullBankDetails,radioValue).subscribe($finalData=>{
          //console.log("Reply Data from server bank details: ",$finalData);
          if($finalData.status==1){
            this.my_class5='overlay1';
            if(radioValue=="agr"){
              this.bankMessage="Thank You For Your Bank Details Confirmation";
              this.disAgrDisabled = true;
              this.agrDisabled =false;
            }
            else if (radioValue=="dgr"){
              this.bankMessage="Please Contact Respective Mandi(APMC) Secretary";
              this.agrDisabled =true;
              this.disAgrDisabled = false;
            }
            setTimeout(()=>{
              this.my_class5 = 'overlay';

            },3000);
            // this.my_class5 = 'overlay';
          }

        });

    }

  }
