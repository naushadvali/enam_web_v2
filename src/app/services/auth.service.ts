import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";
import * as FileSaver from 'file-saver';
import { ResponseContentType } from '@angular/http';
import { environment } from '../../environments/environment';
import { EnvironmentSpecificService } from '../services/environment-specific.service';
import { EnvironmentSpecificResolverService } from '../services/environment-specific-resolver.service';

@Injectable()
export class AuthService {
  user:any;
  sendPreference:any;
  authUrl:any;
  token1:string;
  dateInsert:string;
  //lotcode:String;
  invoiceno:string;
  loginUrl:any;
  logoutUrl:any;
  location:any;
  bidauthUrl:any;
  getpdf:any;
  apmclocation:string;
  agent:string;
  commodity:string;
  lotcode:string;
  seller:string;
  village:string;
  createdInOprID:string;
  loginID:string;
  oprID:string;
  orgID:string;
  userEmailID:string;
  userName:string;
  userPhoneNo:string;
  userRefType:string;
  addprefer:string;
  bidnow:string;
  result:any;
  locationName:any;
  userFullName:any;
  testUrl:any;
  text:any;
  possible:any;



  constructor(private http:Http,private envSpecificSvc: EnvironmentSpecificService,
  private envResSvc: EnvironmentSpecificResolverService) {
    //this.token="435453735ybdgjfa";

    // this.authUrl="http://staging.techl33t.com:8000/enam/api/view/";
    // this.loginUrl="http://staging.techl33t.com:8000/enam/api/login";
    // this.logoutUrl="http://staging.techl33t.com:8000/enam/api/logout";
    // this.bidauthUrl="http://staging.techl33t.com:8000/enam/api/view/newBidList/";
    // this.getpdf="http://staging.techl33t.com:8000/enam/api/view/pdf";


    // this.testUrl="http://192.168.0.125:8080/enam/api/view/";
    // this.authUrl="http://staging.techl33t.com:8181/enam/api/view/";
    // this.loginUrl="http://staging.techl33t.com:8181/enam/api/login";
    // this.logoutUrl="http://staging.techl33t.com:8181/enam/api/logout";
    // this.bidauthUrl="http://staging.techl33t.com:8181/enam/api/view/newBidList/";
    // this.getpdf="http://staging.techl33t.com:8181/enam/api/view/pdf";
    // this.addprefer="http://staging.techl33t.com:8181/enam/api/view/onAddPrefferedCommodity";
    // this.bidnow="http://staging.techl33t.com:8181/enam/api/view/bidNow";



    // this.authUrl="http://10.247.33.159:8080/enam/api/view/";
    // this.loginUrl="http://10.247.33.159:8080/enam/api/login";
    // this.logoutUrl="http://10.247.33.159:8080/enam/api/logout";
    // this.bidauthUrl="http://10.247.33.159:8080/enam/api/view/newBidList/";
    // this.getpdf="http://10.247.33.159:8080/enam/api/view/pdf";
    // this.addprefer="http://10.247.33.159:8080/enam/api/view/onAddPrefferedCommodity";
    // this.bidnow="http://10.247.33.159:8080/enam/api/view/bidNow";

   //this.link1 = this.envSpecificSvc.envSpecific.testUrl;
    // this.testUrl=this.envSpecificSvc.envSpecific.testUrl;
    // this.authUrl=this.envSpecificSvc.envSpecific.authUrl;
    // this.loginUrl=this.envSpecificSvc.envSpecific.loginUrl;
    // this.logoutUrl=this.envSpecificSvc.envSpecific.logoutUrl;
    // this.bidauthUrl=this.envSpecificSvc.envSpecific.bidauthUrl;
    // this.getpdf=this.envSpecificSvc.envSpecific.getpdf;
    // this.addprefer=this.envSpecificSvc.envSpecific.addprefer;
    // this.bidnow=this.envSpecificSvc.envSpecific.bidnow;
  }

  //PENDING LIST
  /*getPendingList(dateInsert,lotcode,invoiceno){
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token);
  headers.append('Content-type','application/json');
  return this.http.get(this.authUrl+'pendingList/'+dateInsert+'&'+lotcode+'&'+invoiceno,{headers:headers})
  .map(res=>res.json())
  .catch((error:any) => Observable.throw('Server error'));
}*/

//login

    loginUser(user){
      //console.log("link in api",this.envSpecificSvc,this.envResSvc);

      let headers = new Headers();
      headers.append('Content-type','application/json');
      // headers.append('Access-Control-Allow-Credentials', 'true');
      // headers.append('Access-Control-Allow-Origin', 'true');

      return this.http.post(this.envSpecificSvc.envSpecific.loginUrl,user,{headers:headers, withCredentials: true})
        .map(res=>res.json())
        .catch((error:any) => {
    		localStorage.setItem('loginError',error.status);
    		return Observable.throw('Server error')});
      //this.result=ret;
      //console.log("Result from service",JSON.parse(this.result._body).data.locationList[0].adoumOprName);
      //this.locationName=JSON.parse(this.result._body).data.locationList[0].adoumOprName;
      //this.userFullName=JSON.parse(this.result._body).data.userName;
    //  console.log("Full Name from service",JSON.parse(this.result._body).data.userName);

    }

    getCapchaUrl() {
      return this.envSpecificSvc.envSpecific.captchaUrl;
    }

    storeUserData(token1, user){
      //console.log("storageData in server",token1);
      this.text = "";
      this.possible = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";

      for (var i = 0; i < 10; i++){
        this.text += this.possible.charAt(Math.floor(Math.random() * this.possible.length));
        //console.log("Random number is :",this.text);
      }
      localStorage.setItem('Rx8CdJBiVK',token1);
      localStorage.setItem('user',JSON.stringify(user));

       this.token1 = token1;
       //console.log("this.token1 in server",this.token1);
      // this.user = user;
    }

    loadToken(){
      //const token1 = sessionStorage.getItem(this.text);
      //console.log("token in load token: ",token1);
      this.token1 = localStorage.getItem('Rx8CdJBiVK');
      //console.log("token in load token: ",this.token1);
    }
    getStorageData(key){
      //console.log("key from server",sessionStorage.getItem(key));
      return localStorage.getItem(key);
    }
    // getAuthUser(){
    //   const user = sessionStorage.getItem('user');
    //   this.user = JSON.parse(user);
    //   return this.user;
    // }
    getAuthUser(){
       if(this.loggedIn()){
         let user = localStorage.getItem("user");
         this.user = JSON.parse(user);
         return this.user;
       }
       else{
         return null;
       }
     }

    loggedIn() {
      if (localStorage.getItem('Rx8CdJBiVK') === null) {
        return false;
      }else{

        return true;
      }
      //return tokenNotExpired('token');
    }
    //LOGOUT
    loggedOut(){
      //sessionStorage.clear();
      this.loadToken();
      //console.log("local storage token in logged out: ",localStorage.getItem('token1'));
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(this.envSpecificSvc.envSpecific.logoutUrl,{headers:headers})
      .map(res=>{res.json()})
      //.catch((error:any) => Observable.throw('Server error'));
      .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }

    //PASSWORD CHANGE
    getPasswordChange(sendPreference){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(this.envSpecificSvc.envSpecific.authUrl+"preferenceSave/",sendPreference,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }




    //PENDING LIST
    getPendingList(fromDate,lotcode,invoiceno){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(this.envSpecificSvc.envSpecific.authUrl+"pendingList/"+lotcode+"&"+invoiceno,{"fromDate":fromDate},{headers:headers})
      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }

    //COMMODITY LIST
    getCommodityList(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(this.envSpecificSvc.envSpecific.authUrl+'commodityList',{headers:headers})
      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });

    }
    //PERMIT REQUESTED
    getPermitReq(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(this.envSpecificSvc.envSpecific.authUrl+'getPermits',{headers:headers})
      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }


    //PERMIT VIEW
    // getPermitView(){
    //   this.loadToken();
    //   let headers = new Headers();
    //   headers.append('Authorization','Bearer '+this.token);
    //   headers.append('Content-type','application/json');
    //   return this.http.get(this.authUrl+'permitsView',{headers:headers})
    //   .map(res=>res.json())
    //   .catch((error:any) => Observable.throw('Server error'));
    // }
    //TRADE VIEW
    getTradeView(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(this.envSpecificSvc.envSpecific.authUrl+'tradeView',{headers:headers})
      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }
    //SECURITY DEPOSIT
    getSecurityDeposit(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(this.envSpecificSvc.envSpecific.authUrl+'securityDeposits',{headers:headers})
      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }
    //OUTCRY
    getOutcry(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(this.envSpecificSvc.envSpecific.authUrl+'outcry',{headers:headers})
      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }
    //BANK DETAILS
    getBankDetails(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(this.envSpecificSvc.envSpecific.authUrl+'bankDetails',{headers:headers})
      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }
    //COMMODITY BID LISTING
    getCommodityBidListing(postJson){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(this.envSpecificSvc.envSpecific.authUrl+'commodityBidListing',postJson,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }
    //COMMODITY BID LISTING Server Side Pagination
    getCommodityBidListingPagination(index,postJson){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(this.envSpecificSvc.envSpecific.authUrl+'commodityBidListingPagi'+"/"+index+"/"+"10",postJson,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }
    //BID REPORT
    getBidreport(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(this.envSpecificSvc.envSpecific.authUrl+'reports',{headers:headers})
      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }
    //GET LOCATION
    getLocation(){
      this.loadToken();
      let headers = new Headers();
      //console.log("Token from Get Location",this.token);
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(this.envSpecificSvc.envSpecific.authUrl+'stateList',{headers:headers})
      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }

    postLocation(location){
      this.loadToken();
      //console.log("Location from Auth Service: ",location);
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(this.envSpecificSvc.envSpecific.authUrl+'apmcList',location,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }

    getAllNewBidListing(apmcOprId,postJson){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(this.envSpecificSvc.envSpecific.bidauthUrl+apmcOprId,postJson,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }
    // get bid listing by offset
    getNewBidListing(apmcOprId,index,limit,postJson){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(this.envSpecificSvc.envSpecific.bidauthUrlPagination+apmcOprId+"/"+index+"/"+"10",postJson,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }

    loadApmcData(apmcdata){
      this.loadToken();
      localStorage.setItem('apmcdata',apmcdata);
      //console.log("storage APMC data from Authservice: ",apmcdata);
    }

    getApmcData(){
      const gotApmcData=('apmcdata');
    }

    getChallanPdf(data){
      console.log("data in challan",data)
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      //headers.append('Content-type','application/pdf');
      return this.http.post(this.envSpecificSvc.envSpecific.getpdf,data,{headers:headers,responseType: ResponseContentType.Blob})
      .map(
        (res) => {
                return new Blob([res.blob()], { type: 'application/pdf' })
            })
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }

    sendPrefData(sendPreference){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(this.envSpecificSvc.envSpecific.loginUrl,sendPreference,{headers:headers}).map(ret=>ret.json());
    }
    addPreferredData(addPrefered){
      this.loadToken();
      let headers = new Headers();
      //console.log("Added for sending data on click add prefred ",addPrefered);
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(this.envSpecificSvc.envSpecific.addprefer,addPrefered,{headers:headers}).map(ret=>ret.json());
    }

    // Get All Agents using APMC List Id
    getAgentList(apmcOprId){
      this.loadToken();
      let headers = new Headers();
      //console.log("Agent from Service",apmcOprId);
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      //console.log("Check url",this.authUrl+"agentList/"+apmcOprId);
      return this.http.get(this.envSpecificSvc.envSpecific.authUrl+"agentList/"+apmcOprId,{headers:headers})

      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }
    //Commodity bidlisting commodity details
    getCommodityListAgent(agentId){
      this.loadToken();
      let headers = new Headers();
      //console.log("Agent from Service",agentId);
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      //console.log("Check url",this.envSpecificSvc.envSpecific.authUrl+"productList/"+agentId);
      return this.http.get(this.envSpecificSvc.envSpecific.authUrl+"productList/"+agentId,{headers:headers})

      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }
    //Bid now button Newbidlisting
    bidNow(bidNow){
      this.loadToken();
      let headers = new Headers();
      //console.log("bid now details from service ",bidNow);
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(this.envSpecificSvc.envSpecific.bidnow,bidNow,{headers:headers}).map(ret=>ret.json());
    }

    //Bidhistory
    getBidHistory(date){
      this.loadToken();
      //console.log("Bidistory from service ",date);
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(this.envSpecificSvc.envSpecific.authUrl+"myBidHistory/",date,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }

    getCustomerdetails(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(this.envSpecificSvc.envSpecific.authUrl+"traderInfo",{headers:headers})
      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }
    //pay now
    postPayment(data){
      this.loadToken();
      console.log("payment from service ",data);
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(this.envSpecificSvc.envSpecific.authUrl+"payNowOnPendingInvoice",data,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }

    accessCode:any;encRequest:any;acc:any;
    getCcaRequest(ccaRequest){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      //console.log("cca request format in server: ",ccaRequest);
      return this.http.post(this.envSpecificSvc.envSpecific.authUrl+"encCcaReq",ccaRequest,{headers:headers})
      .map(res=>{
        res.json();
        this.acc=res;
        this.accessCode=JSON.parse(this.acc).data.accessCode;
        this.encRequest=JSON.parse(this.acc).data.ccaRequest;
      })
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }

    //get Certificate
    getCertificate(abcOprID,abcLotID){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(this.envSpecificSvc.envSpecific.authUrl+"assayingResult/"+abcOprID+'&'+abcLotID,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }
    //get Test Result PDF
    getTradeResult(abcLotID,abcOprID){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/pdf');
      return this.http.get(this.envSpecificSvc.envSpecific.authUrl+"assayingResultPdf/"+abcLotID+'/'+abcOprID,{headers:headers,responseType: ResponseContentType.Blob})
      .map(
        (res) => {
                return new Blob([res.blob()], { type: 'application/pdf' })
            })
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }

    //get online receipt PDF
    getOnlinereceipt(receiptJson){
      console.log("receiptJson",receiptJson)
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(this.envSpecificSvc.envSpecific.authUrl+"paidReportPdf/pdf",receiptJson,{headers:headers,responseType: ResponseContentType.Blob})
      .map(
        (res) => {
                return new Blob([res.blob()], { type: 'application/pdf' })
            })
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }


    //get settlement PDF
    getSettlementresult(settlementJson){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(this.envSpecificSvc.envSpecific.authUrl+"settlementReportPdf/pdf",settlementJson,{headers:headers,responseType: ResponseContentType.Blob})
      .map(
        (res) => {
                return new Blob([res.blob()], { type: 'application/pdf' })
            })
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }

    getReport(date){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.post(this.envSpecificSvc.envSpecific.authUrl+"getReports/",date,{headers:headers})
      .map(res=>res.json())
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }

    getWinnerList(dateJsonWin,reportType){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      headers.append('Accept','application/pdf');
      return this.http.post(this.envSpecificSvc.envSpecific.testUrl+"traderWinnerList/"+reportType,dateJsonWin,{headers:headers,responseType: ResponseContentType.Blob})

      .map(
        (res) => {
                //console.log("response",res);
                return new Blob([res.blob()], { type: 'application/pdf' })
            })
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
          }

      });

    }


    getWinnerListRtf(dateJsonWin,reportType){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      headers.append('Accept','application/pdf');
      return this.http.post(this.envSpecificSvc.envSpecific.testUrl+"traderWinnerList/"+reportType,dateJsonWin,{headers:headers,responseType: ResponseContentType.Blob})

      .map(
        (res) => {
                //console.log("response",res);
                return new Blob([res.blob()], { type: 'application/rtf' })
            })
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
          }

      });

    }

    getWinnerListExcel(dateJsonWin,reportType){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      headers.append('Accept','application/pdf');
      return this.http.post(this.envSpecificSvc.envSpecific.testUrl+"traderWinnerList/"+reportType,dateJsonWin,{headers:headers,responseType: ResponseContentType.Blob})

      .map(
        (res) => {
                //console.log("response",res);
                return new Blob([res.blob()], { type: 'application/vnd.ms-excel' })
            })
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
          }

      });

    }

    getTraderLotPdf(dateJsonTrader,lotFileType){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      headers.append('Accept','application/pdf');
      return this.http.post(this.envSpecificSvc.envSpecific.testUrl+"traderCALotDetails/"+lotFileType,dateJsonTrader,{headers:headers,responseType: ResponseContentType.Blob})

      .map(
        (res) => {
                //console.log("response",res);
                return new Blob([res.blob()], { type: 'application/pdf' })
            })
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
          }

      });

    }

    getTraderLotExcel(dateJsonTrader,lotFileType){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      headers.append('Accept','application/pdf');
      return this.http.post(this.envSpecificSvc.envSpecific.testUrl+"traderCALotDetails/"+lotFileType,dateJsonTrader,{headers:headers,responseType: ResponseContentType.Blob})

      .map(
        (res) => {
                //console.log("response",res);
                return new Blob([res.blob()], { type: 'application/vnd.ms-excel'})
            })
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
          }

      });

    }

    getTraderLotRtf(dateJsonTrader,lotFileType){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      headers.append('Accept','application/pdf');
      return this.http.post(this.envSpecificSvc.envSpecific.testUrl+"traderCALotDetails/"+lotFileType,dateJsonTrader,{headers:headers,responseType: ResponseContentType.Blob})

      .map(
        (res) => {
                //console.log("response",res);
                return new Blob([res.blob()], { type: 'application/rtf' })
            })
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
          }

      });

    }


    //get server time
    serverDate:string;
    getServerTime(){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      return this.http.get(this.envSpecificSvc.envSpecific.authUrl+"currentDate/",{headers:headers})
      .map(
        ret=>{
          ret.json()
          this.serverDate = ret.json().message;
        }
      )
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
      }

  });
    }


    sendBankAgrDgr(fullBankDetails,agrdgrVal){
            this.loadToken();
            let headers = new Headers();
            headers.append('Authorization','Bearer '+this.token1);
            headers.append('Content-type','application/json');
            return this.http.post(this.envSpecificSvc.envSpecific.authUrl+"updateBankDetailForTrader/"+agrdgrVal,fullBankDetails,{headers:headers})
            .map(res=>res.json())
            .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }

        });
    }


    getSaleAgreementPdf(dateJsonTrader,lotFileType){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      headers.append('Accept','application/pdf');
      return this.http.post(this.envSpecificSvc.envSpecific.testUrl+"onSaleAgreement/"+lotFileType,dateJsonTrader,{headers:headers,responseType: ResponseContentType.Blob})

      .map(
        (res) => {
                //console.log("response",res);
                return new Blob([res.blob()], { type: 'application/pdf' })
            })
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
          }

      });

    }

    getSaleAgreementExcel(dateJsonTrader,lotFileType){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      headers.append('Accept','application/pdf');
      return this.http.post(this.envSpecificSvc.envSpecific.testUrl+"onSaleAgreement/"+lotFileType,dateJsonTrader,{headers:headers,responseType: ResponseContentType.Blob})

      .map(
        (res) => {
                //console.log("response",res);
                return new Blob([res.blob()], { type: 'application/vnd.ms-excel'})
            })
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
          }

      });

    }

    getSaleAgreementRtf(dateJsonTrader,lotFileType){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      headers.append('Accept','application/pdf');
      return this.http.post(this.envSpecificSvc.envSpecific.testUrl+"onSaleAgreement/"+lotFileType,dateJsonTrader,{headers:headers,responseType: ResponseContentType.Blob})

      .map(
        (res) => {
                //console.log("response",res);
                return new Blob([res.blob()], { type: 'application/rtf' })
            })
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
          }
      });
    }

// Get sale bill report
    getSaleBillPdf(dateJsonTrader,lotFileType){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      headers.append('Accept','application/pdf');
      return this.http.post(this.envSpecificSvc.envSpecific.testUrl+"onSaleBillReport/"+lotFileType,dateJsonTrader,{headers:headers,responseType: ResponseContentType.Blob})

      .map(
        (res) => {
                //console.log("response",res);
                return new Blob([res.blob()], { type: 'application/pdf' })
            })
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
          }

      });

    }

    getSaleBillExcel(dateJsonTrader,lotFileType){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      headers.append('Accept','application/pdf');
      return this.http.post(this.envSpecificSvc.envSpecific.testUrl+"onSaleBillReport/"+lotFileType,dateJsonTrader,{headers:headers,responseType: ResponseContentType.Blob})

      .map(
        (res) => {
                //console.log("response",res);
                return new Blob([res.blob()], { type: 'application/vnd.ms-excel'})
            })
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
          }

      });

    }

    getSaleBillRtf(dateJsonTrader,lotFileType){
      this.loadToken();
      let headers = new Headers();
      headers.append('Authorization','Bearer '+this.token1);
      headers.append('Content-type','application/json');
      headers.append('Accept','application/pdf');
      return this.http.post(this.envSpecificSvc.envSpecific.testUrl+"onSaleBillReport/"+lotFileType,dateJsonTrader,{headers:headers,responseType: ResponseContentType.Blob})

      .map(
        (res) => {
                //console.log("response",res);
                return new Blob([res.blob()], { type: 'application/rtf' })
            })
      .catch(e => {
      if (e.status === 401) {
          return Observable.throw('Unauthorized');
          }
      });
    }


// Get State List Here
getStateList(){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(this.envSpecificSvc.envSpecific.authUrl+"stateList/",{headers:headers})
  .map(res=>res.json())
  .catch(e => {
  if (e.status === 401) {
      return Observable.throw('Unauthorized');
  }
});
}

// Get APMC List
getAPMCList(stateID){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(this.envSpecificSvc.envSpecific.authUrl+"apmcList/"+stateID,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
  if (e.status === 401) {
      return Observable.throw('Unauthorized');
  }
});
}
// Get Agent Name List
getAgentNameList(stateID){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(this.envSpecificSvc.envSpecific.authUrl+"newBidListAgentName/"+stateID,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
  if (e.status === 401) {
      return Observable.throw('Unauthorized');
  }
});
}
// Get Commodity Name List for new bid listing
getNewBidListingCommList(stateID){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(this.envSpecificSvc.envSpecific.authUrl+"newBidListCommName/"+stateID,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
  if (e.status === 401) {
      return Observable.throw('Unauthorized');
  }
});
}
// Get Commodity Name List for all commodity bid listing
getAllCommodityList(){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(this.envSpecificSvc.envSpecific.authUrl+"commodityBidListingCommName/",{headers:headers})
  .map(res=>res.json())
  .catch(e => {
  if (e.status === 401) {
      return Observable.throw('Unauthorized');
  }
});
}
// Validate password
validatePassword(postJson){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.post(this.envSpecificSvc.envSpecific.authUrl+"validatePassword/",postJson,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
  if (e.status === 401) {
      return Observable.throw('Unauthorized');
  }
});
}


// get bid listing
getMyBidValue(apmcOprId,lotCode){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(this.envSpecificSvc.envSpecific.authUrl+"lastBidValue/"+lotCode+"/"+apmcOprId,{headers:headers})
  .map(res=>res.json())
  .catch(e => {
  if (e.status === 401) {
      return Observable.throw('Unauthorized');
  }

});
}


//get last bid value
maxOpenBidMsg:any=[];
getLastBidValue(apmcOprId,lotCode,index){
  this.loadToken();
  let headers = new Headers();
  headers.append('Authorization','Bearer '+this.token1);
  headers.append('Content-type','application/json');
  return this.http.get(this.envSpecificSvc.envSpecific.authUrl+"maxBid/"+lotCode+"/"+apmcOprId,{headers:headers})
  .map(
    ret=>{
      ret.json()
      this.maxOpenBidMsg[index] = ret.json().message;
    }
  )
  .catch(e => {
  if (e.status === 401) {
      return Observable.throw('Unauthorized');
  }

});
}

}
