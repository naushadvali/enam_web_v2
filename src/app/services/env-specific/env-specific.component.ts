import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-env-specific',
  templateUrl: './env-specific.component.html',
  styleUrls: ['./env-specific.component.css']
})
export class EnvSpecificComponent implements OnInit {

  constructor() { }
  testUrl: string;
  authUrl: string;
  loginUrl: string;
  captchaUrl: string;
  logoutUrl: string;
  bidauthUrl: string;
  bidauthUrlPagination: string;
  getpdf: string;
  addprefer: string;
  bidnow: string;
  getMyBidValue: string;
  eng: string;
  hi: string;
  gu: string;
  te: string;
  bn: string;
  mr: string;
  ngOnInit() {
  }

}
