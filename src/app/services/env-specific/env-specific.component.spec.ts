import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnvSpecificComponent } from './env-specific.component';

describe('EnvSpecificComponent', () => {
  let component: EnvSpecificComponent;
  let fixture: ComponentFixture<EnvSpecificComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnvSpecificComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnvSpecificComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
