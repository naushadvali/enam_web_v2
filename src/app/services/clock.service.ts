import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
@Injectable()
export class ClockService {

  private clock: Observable<Date>;
  myDate:any;
  constructor() {
    this.myDate=new Date();
    //console.log("checking new Date()",this.myDate);
    this.clock = Observable.interval(1000).map(tick => new Date()).share();
    //console.log("clock1 is",this.clock);


    //   setInterval(() => {         //replaced function() by ()=>
    //   this.myDate = new Date();
    //   console.log(this.myDate); // just testing if it is working
    // }, 1000);

  }

  getClock(): Observable<Date> {
    //console.log("clock2 is",this.clock);
    return this.clock;
  }

}
