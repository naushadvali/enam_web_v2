import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot,
         ActivatedRouteSnapshot } from '@angular/router';

import { EnvSpecificComponent } from './env-specific/env-specific.component';
import { EnvironmentSpecificService } from './environment-specific.service';

@Injectable()
export class EnvironmentSpecificResolverService {

  constructor(private envSpecificSvc: EnvironmentSpecificService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<EnvSpecificComponent> {
      return this.envSpecificSvc.loadEnvironment()
              .then(es => {
                  this.envSpecificSvc.setEnvSpecific(es);
                  return this.envSpecificSvc.envSpecific;
              }, error => {
                  console.log(error);
                  return null;
              });
    }
}
