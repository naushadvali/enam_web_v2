import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";
import * as FileSaver from 'file-saver';
import { ResponseContentType } from '@angular/http';
import { environment } from '../../environments/environment';
import { EnvironmentSpecificService } from '../services/environment-specific.service';
import { EnvironmentSpecificResolverService } from '../services/environment-specific-resolver.service';
import { map, catchError, finalize } from 'rxjs/operators';
@Injectable()
export class LogisticsService {

   baseUrl = "http://192.168.0.144:8080/enam/api/view";
   token:any;

   constructor(
      private http:Http
    ) { }
    
    loadToken(){
      this.token = localStorage.getItem('Rx8CdJBiVK');
    }

    getData(): Observable<any>{
      let headers = new Headers();
      this.loadToken();
      headers.append('Authorization','Bearer '+this.token); 
      return this.http.get(this.baseUrl+"/logistics",{headers:headers}).pipe(map(data => {
          return JSON.parse(data['_body']);
      }));
    }

    getRating(json){
      let headers = new Headers();
      headers.append('Authorization','Bearer 4438lpl2ardf6k5bcitsoonk0hpaeoov');
      return this.http.post(this.baseUrl+"/logistics-rating",json,{headers:headers}).pipe(map(ret=>{
        return JSON.parse(ret['_body']);
      }));

    }

    

}
