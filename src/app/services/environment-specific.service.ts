
import { Injectable, OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable, Subscription, BehaviorSubject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { EnvSpecificComponent } from './env-specific/env-specific.component';

@Injectable()
export class EnvironmentSpecificService {
   public envSpecific: EnvSpecificComponent;
   public envSpecificNull: EnvSpecificComponent = null;
   private envSpecificSubject: BehaviorSubject<EnvSpecificComponent> = new BehaviorSubject<EnvSpecificComponent>(null);
   constructor(private http: Http) {
  }
  public loadEnvironment() {
      // Only want to do this once - if root page is revisited, it calls this again.
      if (this.envSpecific === null || this.envSpecific === undefined) {
        return this.http.get('./assets/env-specific.json')
            .map((data) => data.json())
            .toPromise<EnvSpecificComponent>();
      }

      return Promise.resolve(this.envSpecificNull);
  }

  public setEnvSpecific(es: EnvSpecificComponent) {
    // This has already been set so bail out.
    if (es === null || es === undefined) {
        return;
    }

    this.envSpecific = es;
    if (this.envSpecificSubject) {
        this.envSpecificSubject.next(this.envSpecific);
    }
  }

  /*
    Call this if you want to know when EnvSpecific is set.
  */
  public subscribe(caller: any, callback: (caller: any, es: EnvSpecificComponent) => void) {
      this.envSpecificSubject
          .subscribe((es) => {
              if (es === null) {
                  return;
              }
              callback(caller, es);
          });
  }
}
