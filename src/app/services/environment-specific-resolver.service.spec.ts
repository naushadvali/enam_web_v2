import { TestBed, inject } from '@angular/core/testing';

import { EnvironmentSpecificResolverService } from './environment-specific-resolver.service';

describe('EnvironmentSpecificResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnvironmentSpecificResolverService]
    });
  });

  it('should be created', inject([EnvironmentSpecificResolverService], (service: EnvironmentSpecificResolverService) => {
    expect(service).toBeTruthy();
  }));
});
