import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes} from '@angular/router';
import { DataTableModule } from "angular2-datatable";
import { MyDatePickerModule } from 'mydatepicker';
import { ReCaptchaModule } from 'angular2-recaptcha';
import { LocalStorageModule } from 'angular-2-local-storage';
import { SelectModule } from 'ng2-select';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { DOCUMENT } from '@angular/platform-browser';
import { MdTooltipModule,MdButtonModule, MdCheckboxModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive'; // this includes the core NgIdleModule but includes keepalive providers for easy wireup
import { MomentModule } from 'angular2-moment'; // optional, provides moment-style pipes for date formatting
import { LoadingModule} from 'ngx-loading';
import { FilterPipe} from './pipes/filter.pipe';
import { environment } from '../environments/environment';

//services
import { AuthGuard } from './guards/auth.guards';
import { AuthService } from './services/auth.service';
import { ClockService } from './services/clock.service';
import { ValidateService } from './services/validate.service';
import { LogisticsService } from './services/logistics.service';

//components

import { AppComponent } from './app.component';
import { BankdetailsComponent } from './bankdetails/bankdetails.component';
import { BidhistoryComponent } from './bidhistory/bidhistory.component';
import { TimerComponent } from './timer/timer.component';
import { ReportComponent } from './report/report.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavbarComponent} from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NewbidlistingComponent } from './newbidlisting/newbidlisting.component';
import { CommoditybidlistingComponent } from './commoditybidlisting/commoditybidlisting.component';
import { EnvironmentSpecificResolverService } from './services/environment-specific-resolver.service';
import { EnvironmentSpecificService } from './services/environment-specific.service';
import { EnvSpecificComponent } from './services/env-specific/env-specific.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { LogisticsservicesComponent } from './logisticsservices/logisticsservices.component';
import { PermitComponent } from './permit/permit.component';
import { NewpermitComponent } from './newpermit/newpermit.component';

//Routing

export const appRoutes: Routes =[
  {path:'login',component:LoginComponent,resolve: { envSpecific: EnvironmentSpecificResolverService }},
  {path:'',component:LoginComponent,resolve: { envSpecific: EnvironmentSpecificResolverService }},
  {path:'bankdetails',component:BankdetailsComponent,canActivate: [AuthGuard],resolve: { envSpecific: EnvironmentSpecificResolverService }},
  {path:'bidhistory',component:BidhistoryComponent,canActivate: [AuthGuard],resolve: { envSpecific: EnvironmentSpecificResolverService }},
  {path:'newbidlisting',component:NewbidlistingComponent,canActivate: [AuthGuard],resolve: { envSpecific: EnvironmentSpecificResolverService }},
  {path:'commoditybidlisting',component:CommoditybidlistingComponent,canActivate: [AuthGuard],resolve: { envSpecific: EnvironmentSpecificResolverService }},
  {path:'report',component:ReportComponent,canActivate: [AuthGuard],resolve: { envSpecific: EnvironmentSpecificResolverService }},
  {path:'logistics',component:LogisticsservicesComponent,canActivate: [AuthGuard],resolve: { envSpecific: EnvironmentSpecificResolverService }},
  {path:'permit',component:PermitComponent,canActivate: [AuthGuard],resolve: { envSpecific: EnvironmentSpecificResolverService }},
  {path:'newpermit',component:NewpermitComponent,canActivate: [AuthGuard],resolve: { envSpecific: EnvironmentSpecificResolverService }},
  {path:'dashboard',component:DashboardComponent,canActivate: [AuthGuard],resolve: { envSpecific: EnvironmentSpecificResolverService }}
]
;

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    NavbarComponent,
    BankdetailsComponent,
    BidhistoryComponent,
    NewbidlistingComponent,
    CommoditybidlistingComponent,
    ReportComponent,
    LoginComponent,
    SidebarComponent,
    NewbidlistingComponent,
    CommoditybidlistingComponent,
    TimerComponent,
    EnvSpecificComponent,
    LogisticsservicesComponent,
    PermitComponent,
    FilterPipe,
    NewpermitComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    DataTableModule,
    MyDatePickerModule,
    ReCaptchaModule,
    LocalStorageModule,
    SelectModule,
    FlashMessagesModule,
    MdTooltipModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    LoadingModule,
    MomentModule,
    NgIdleKeepaliveModule.forRoot(),
    NgxPaginationModule,
    InfiniteScrollModule
  ],
  providers: [AuthService,ValidateService,ClockService,LogisticsService,AuthGuard,EnvironmentSpecificResolverService,EnvironmentSpecificService],
  bootstrap: [AppComponent]
})
export class AppModule { }
