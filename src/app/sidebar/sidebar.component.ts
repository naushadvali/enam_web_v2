import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { MdTooltipModule,MdButtonModule, MdCheckboxModule } from '@angular/material';


@Component({
    moduleId: module.id,
    selector: 'sidebar',
    templateUrl: 'sidebar.component.html',
    styleUrls: ['sidebar.component.css']
})
export class SidebarComponent {
  constructor(public authservice:AuthService){
  //console.log("In side bar");
  }

}
