import { Component , ViewChild, ElementRef, AfterViewInit,OnInit} from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../services/auth.service';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
@Component({
  selector: 'report',
  templateUrl: 'report.component.html',
  styleUrls: ['report.component.css']
})
export class ReportComponent implements OnInit {
  data=[];
  bidReportData=[];
  reportFromDate:string;
  reportToDate:string;
  call0:string;
  call1:string;
  call2:string;
  dateInsert:any;
  dateChange:any;
  monthChnage:any;
  dateChange1:any;
  monthChnage1:any;
  fromDate:string;
  toDate:string;
  reportData=[];
  payOrgID:any;
  payAggrOprID:any;
  payTrdID:any;
  payInvoiceNo:any;
  payAggrNo:any;
  payInvoiceAmt:any;
  payMode:any;
  farmerName:any;
  lotCode:any;
  agentName:any;
  payOrdID:any;
  auchInvoiceDate:any;
  fileType:any;
  stateID:any;
  apmcID:any;
  public my_class50:any='overlay';

  stateListData=[];
  apmcListData=[];
  userGlobal:string;
  selectedState:boolean;
  stateSel:any;
  apmcSel:any;


  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private flashMessagesService: FlashMessagesService,
    private router:Router,
  ) {
    this.fileType="pdf";
    this.selectedState=false;


    }

  ngOnInit() {
    this.userGlobal = localStorage.getItem("userGlobal");
    this.stateID = localStorage.getItem("stateID");
    this.apmcID = localStorage.getItem("apmcID");
    this.fetchStatelist(this.stateID);

  }

  getReport(){

    if(this.reportFromDate==undefined && this.reportToDate==undefined){
      this.flashMessagesService.show('Dates are mandatory to search .', { cssClass: 'alert-danger', timeout: 3000 });

    }else{


      //for from date
      const reportFromDate={
        day:this.reportFromDate["date"]["day"],
        month:this.reportFromDate["date"]["month"],
        year:this.reportFromDate["date"]["year"]
      }



      this.dateChange=this.reportFromDate["date"]["day"];
      for(var i=1;i<=9;i++){
        if(this.dateChange==i){
          let zero=0;
          let x:any;
          let y:any;

          x=zero.toString()+(this.dateChange).toString();
          this.dateChange=x;
        }
      }
      this.monthChnage=this.reportFromDate["date"]["month"];

      for(var j=1;j<=9;j++){
        if(this.monthChnage==j){
          let zero1=0;
          let m:any;

          m=zero1.toString()+(this.monthChnage).toString();

          this.monthChnage=m;
        }
      }

      this.fromDate=this.dateChange+"/"+this.monthChnage+"/"+this.reportFromDate["date"]["year"];


      //For to date
      const reportToDate={
        day:this.reportToDate["date"]["day"],
        month:this.reportToDate["date"]["month"],
        year:this.reportToDate["date"]["year"]
      }


      this.dateChange1=this.reportToDate["date"]["day"];
      for(var i=1;i<=9;i++){
        if(this.dateChange1==i){
          let zero=0;
          let x:any;
          let y:any;

          x=zero.toString()+(this.dateChange1).toString();
          this.dateChange1=x;
        }
      }
      this.monthChnage1=this.reportToDate["date"]["month"];

      for(var j=1;j<=9;j++){
        if(this.monthChnage1==j){
          let zero1=0;
          let m:any;


          m=zero1.toString()+(this.monthChnage1).toString();

          this.monthChnage1=m;
        }
      }

      this.fromDate=this.dateChange+"/"+this.monthChnage+"/"+this.reportFromDate["date"]["year"];

      this.toDate=this.dateChange1+"/"+this.monthChnage1+"/"+this.reportToDate["date"]["year"];



      const dateJson={
        fromDate:this.fromDate,
        toDate:this.toDate
      }
      this.authservice.getReport(dateJson).subscribe
      (
        $data=>{
          //console.log("report data are",$data.listData);
          this.reportData=$data.listData;
          let payOrgID;
          let payAggrOprID;
          let payTrdID;
          let payInvoiceNo;
          let payAggrNo;
          let payInvoiceAmt;
          let payMode;
          let farmerName;
          let lotCode;
          let agentName;
          let payOrdID;
          let auchInvoiceDate;

          for(var i=0;i<this.reportData.length;i++){
            payOrgID=this.reportData[i].payOrgID;
            payAggrOprID=this.reportData[i].payAggrOprID;
            payTrdID=this.reportData[i].payTrdID;
            payInvoiceNo=this.reportData[i].payInvoiceNo;
            payAggrNo=this.reportData[i].payAggrNo;
            payInvoiceAmt=this.reportData[i].payInvoiceAmt;
            payMode=this.reportData[i].payMode;
            farmerName=this.reportData[i].farmerName;
            lotCode=this.reportData[i].lotCode;
            agentName=this.reportData[i].agentName;
            payOrdID=this.reportData[i].payOrdID;
            auchInvoiceDate=this.reportData[i].auchInvoiceDate;
          }
          this.payOrgID=payOrgID;
          this.payAggrOprID=payAggrOprID;
          this.payTrdID=payTrdID;
          this.payInvoiceNo=payInvoiceNo;
          this.payAggrNo=payAggrNo;
          this.payInvoiceAmt=payInvoiceAmt;
          this.payMode=payMode;
          this.farmerName=farmerName;
          this.lotCode=lotCode;
          this.agentName=agentName;
          this.payOrdID=payOrdID;
          this.auchInvoiceDate=auchInvoiceDate;

        }, (err) => {
          if (err === 'Unauthorized')
          {
            this.errorPopupShow();
            setTimeout(()=>{
              this.my_class50 = 'overlay';
              localStorage.clear();
              this.router.navigateByUrl('/login');
            },3000);
          }
        }
      );
    }


  }

  //WinnerList PDF
  getWinnerList(){
    console.log("file type",this.fileType)
    if(this.reportFromDate==undefined && this.reportToDate==undefined){
      this.flashMessagesService.show('Date is mandatory to search .', { cssClass: 'alert-danger', timeout: 3000 });

    }
    else{
      if(this.reportFromDate != undefined && this.reportToDate != undefined){
        console.log("date not undefined",this.reportFromDate,this.reportToDate);
      //for from date
      const reportFromDate={
        day:this.reportFromDate["date"]["day"],
        month:this.reportFromDate["date"]["month"],
        year:this.reportFromDate["date"]["year"]
      }


      this.dateChange=this.reportFromDate["date"]["day"];
      for(var i=1;i<=9;i++){
        if(this.dateChange==i){
          let zero=0;
          let x:any;
          let y:any;

          x=zero.toString()+(this.dateChange).toString();
          this.dateChange=x;
        }
      }
      this.monthChnage=this.reportFromDate["date"]["month"];

      for(var j=1;j<=9;j++){
        if(this.monthChnage==j){
          let zero1=0;
          let m:any;

          m=zero1.toString()+(this.monthChnage).toString();

          this.monthChnage=m;
        }
      }

      this.fromDate=this.dateChange+"/"+this.monthChnage+"/"+this.reportFromDate["date"]["year"];
      //console.log("from datde",this.fromDate);

      //For to date
      const reportToDate={
        day:this.reportToDate["date"]["day"],
        month:this.reportToDate["date"]["month"],
        year:this.reportToDate["date"]["year"]
      }


      this.dateChange1=this.reportToDate["date"]["day"];
      for(var i=1;i<=9;i++){
        if(this.dateChange1==i){
          let zero=0;
          let x:any;
          let y:any;

          x=zero.toString()+(this.dateChange1).toString();
          this.dateChange1=x;
        }
      }
      this.monthChnage1=this.reportToDate["date"]["month"];

      for(var j=1;j<=9;j++){
        if(this.monthChnage1==j){
          let zero1=0;
          let m:any;


          m=zero1.toString()+(this.monthChnage1).toString();

          this.monthChnage1=m;
        }
      }

      //this.fromDate=this.dateChange+"/"+this.monthChnage+"/"+this.reportToDate["date"]["year"];
      //console.log("from date:",this.fromDate);
      this.toDate=this.dateChange1+"/"+this.monthChnage1+"/"+this.reportToDate["date"]["year"];

      if(this.apmcSel != undefined){
        if(this.apmcSel.length == 0){
          if(this.apmcListData[0] != undefined){
            this.apmcSel = this.apmcListData[0];
          }
        }
      }


      const dateJsonWin={
        "dateVO":{
            fromDate:this.fromDate,
            toDate:this.toDate},
        "stateVO": this.stateSel,
	      "apmcVO": this.apmcSel
      }


      if(this.fileType=="pdf"){
        console.log("pdf1",dateJsonWin);
        const reportType=this.fileType;
        this.authservice.getWinnerList(dateJsonWin,reportType).subscribe(
          (ret) => {
            var fileURL1 = URL.createObjectURL(ret);
            window.open(fileURL1);
          }, (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
              setTimeout(()=>{
                this.my_class50 = 'overlay';
                localStorage.clear();
                this.router.navigateByUrl('/login');
              },3000);
            }
          }
        );
      }
      else if(this.fileType=="rtf"){
        const reportType=this.fileType;
        this.authservice.getWinnerListRtf(dateJsonWin,reportType).subscribe(
          (ret) => {
            var fileURL1 = URL.createObjectURL(ret);
            window.open(fileURL1);
          }, (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
              setTimeout(()=>{
                this.my_class50 = 'overlay';
                localStorage.clear();
                this.router.navigateByUrl('/login');
              },3000);
            }
          }
        );
      }
      else if(this.fileType=="xls"){
        const reportType=this.fileType;
        this.authservice.getWinnerListExcel(dateJsonWin,reportType).subscribe(
          (ret) => {
            // var fileURL3 = URL.createObjectURL(ret);
            // window.open(fileURL3);
            let blob = ret;
            let a = document.createElement("a");
            a.href = URL.createObjectURL(blob);
            a.download = 'winnerList.xls';
            document.body.appendChild(a);
            a.click();



          }, (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
              setTimeout(()=>{
                this.my_class50 = 'overlay';
                localStorage.clear();
                this.router.navigateByUrl('/login');
              },3000);
            }
          }
        );
      }
      else if(this.fileType==null || this.fileType=='' || this.fileType==undefined){
        this.flashMessagesService.show('Please Choose A File Type', { cssClass: 'alert-danger', timeout: 3000 });
      }
}
  else{
    console.log("date undefined",this.reportFromDate,this.reportToDate);
    this.flashMessagesService.show('Please Enter Both Dates First', { cssClass: 'alert-danger', timeout: 3000 });

  }
    }

  }

  //TraderLot PDF
  getTraderLot(){
    if(this.reportFromDate==undefined && this.reportToDate==undefined){
      this.flashMessagesService.show('Date is mandatory to search .', { cssClass: 'alert-danger', timeout: 3000 });

    }
    else{
      if(this.reportFromDate != undefined && this.reportToDate != undefined){

      //for from date
      const reportFromDate={
        day:this.reportFromDate["date"]["day"],
        month:this.reportFromDate["date"]["month"],
        year:this.reportFromDate["date"]["year"]
      }


      this.dateChange=this.reportFromDate["date"]["day"];
      for(var i=1;i<=9;i++){
        if(this.dateChange==i){
          let zero=0;
          let x:any;
          let y:any;

          x=zero.toString()+(this.dateChange).toString();
          this.dateChange=x;
        }
      }
      this.monthChnage=this.reportFromDate["date"]["month"];

      for(var j=1;j<=9;j++){
        if(this.monthChnage==j){
          let zero1=0;
          let m:any;

          m=zero1.toString()+(this.monthChnage).toString();

          this.monthChnage=m;
        }
      }

      this.fromDate=this.dateChange+"/"+this.monthChnage+"/"+this.reportFromDate["date"]["year"];


      //For to date
      const reportToDate={
        day:this.reportToDate["date"]["day"],
        month:this.reportToDate["date"]["month"],
        year:this.reportToDate["date"]["year"]
      }


      this.dateChange1=this.reportToDate["date"]["day"];
      for(var i=1;i<=9;i++){
        if(this.dateChange1==i){
          let zero=0;
          let x:any;
          let y:any;

          x=zero.toString()+(this.dateChange1).toString();
          this.dateChange1=x;
        }
      }
      this.monthChnage1=this.reportToDate["date"]["month"];

      for(var j=1;j<=9;j++){
        if(this.monthChnage1==j){
          let zero1=0;
          let m:any;


          m=zero1.toString()+(this.monthChnage1).toString();

          this.monthChnage1=m;
        }
      }

      //this.fromDate=this.dateChange+"/"+this.monthChnage+"/"+this.reportToDate["date"]["year"];

      this.toDate=this.dateChange1+"/"+this.monthChnage1+"/"+this.reportToDate["date"]["year"];




      if(this.apmcSel.length == 0){
        if(this.apmcListData[0] != undefined){
          this.apmcSel = this.apmcListData[0];
        }
      }

      const dateJsonTrader={
        "dateVO":{
            fromDate:this.fromDate,
            toDate:this.toDate},
        "stateVO": this.stateSel,
        "apmcVO": this.apmcSel
      }


      if(this.fileType=="pdf"){
        const lotFileType=this.fileType;
        this.authservice.getTraderLotPdf(dateJsonTrader,lotFileType).subscribe(
          (ret) => {
            var fileURL2 = URL.createObjectURL(ret);
            window.open(fileURL2);
          }, (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
              setTimeout(()=>{
                this.my_class50 = 'overlay';
                localStorage.clear();
                this.router.navigateByUrl('/login');
              },3000);
            }
          }
        );
      }
      else if(this.fileType=="rtf"){
        const lotFileType=this.fileType;
        this.authservice.getTraderLotRtf(dateJsonTrader,lotFileType).subscribe(
          (ret) => {
            var fileURL2 = URL.createObjectURL(ret);
            window.open(fileURL2);
          }, (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
              setTimeout(()=>{
                this.my_class50 = 'overlay';
                localStorage.clear();
                this.router.navigateByUrl('/login');
              },3000);
            }
          }
        );
      }
      else if(this.fileType=="xls"){
        const lotFileType=this.fileType;
        this.authservice.getTraderLotExcel(dateJsonTrader,lotFileType).subscribe(
          (ret) => {
            // var fileURL3 = URL.createObjectURL(ret);
            // window.open(fileURL3);

            let blob = ret;
            let a = document.createElement("a");
            a.href = URL.createObjectURL(blob);
            a.download = 'lotDetails.xls';
            document.body.appendChild(a);
            a.click();



          }, (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
              setTimeout(()=>{
                this.my_class50 = 'overlay';
                localStorage.clear();
                this.router.navigateByUrl('/login');
              },3000);
            }
          }
        );
      }
      else{
        this.flashMessagesService.show('Please Choose A File Type', { cssClass: 'alert-danger', timeout: 3000 });
      }
}
      else{
        console.log("date undefined",this.reportFromDate,this.reportToDate);
        this.flashMessagesService.show('Please Enter Both Dates First', { cssClass: 'alert-danger', timeout: 3000 });

      }



    }

  }

  //Online receipt button
  onReceipt(post:any){
    const receiptJson={
      payOrgID:post.payOrgID,
      payAggrOprID:post.payAggrOprID,
      payTrdID:post.payTrdID,
      payInvoiceNo:post.payInvoiceNo,
      payAggrNo:post.payAggrNo,
      payInvoiceAmt:post.payInvoiceAmt,
      payMode:post.payMode,
      farmerName:post.farmerName,
      lotCode:post.lotCode,
      agentName:post.agentName,
      payOrdID:post.payOrdID,
      auchInvoiceDate:post.auchInvoiceDate,
    }
    console.log("post receiptJson",receiptJson)
    this.authservice.getOnlinereceipt(post).subscribe(res=>{
      //console.log("Online receipt data",res);
      var fileURL = URL.createObjectURL(res);
      window.open(fileURL);
    }, (err) => {
      if (err === 'Unauthorized')
      {
        this.errorPopupShow();
        setTimeout(()=>{
          this.my_class50 = 'overlay';
          localStorage.clear();
          this.router.navigateByUrl('/login');
        },3000);
      }
    }
  )
}


//Settlement button
onSettlement(post:any){
  const settlementJson={
    payOrgID:post.payOrgID,
    payAggrOprID:post.payAggrOprID,
    payTrdID:post.payTrdID,
    payInvoiceNo:post.payInvoiceNo,
    payAggrNo:post.payAggrNo,
    payInvoiceAmt:post.payInvoiceAmt,
    payMode:post.payMode,
    farmerName:post.farmerName,
    lotCode:post.lotCode,
    agentName:post.agentName,
    payOrdID:post.payOrdID,
    auchInvoiceDate:post.auchInvoiceDate,
  }


  this.authservice.getSettlementresult(settlementJson).subscribe(ret=>{
    //console.log("Settlement report data",ret);
    var fileURL = URL.createObjectURL(ret);
    window.open(fileURL);
  }, (err) => {
    if (err === 'Unauthorized')
    {
      this.errorPopupShow();
      setTimeout(()=>{
        this.my_class50 = 'overlay';
        localStorage.clear();
        this.router.navigateByUrl('/login');
      },3000);
    }
  }
)



}

public myDatePickerOptions: IMyDpOptions = {
  // other options...
  dateFormat: 'dd-mm-yyyy',
  editableDateField:false,
  openSelectorOnInputClick:true,
  inline:false
};

chooseFileType(fileType:any){
  this.fileType=fileType;
  //console.log("Choosed File type is: ",this.fileType);
  // if(this.fileType=="pdf"){
  //
  // }
  // else if(this.fileType=="rtf"){
  //
  // }
  // else if(this.fileType=="xls"){
  //
  // }
}

errorPopupShow(){
  this.my_class50='overlay1';
}



// Get Sale agreement
getSaleAgreement(){
  if(this.reportFromDate==undefined && this.reportToDate==undefined){
    this.flashMessagesService.show('Date is mandatory to search .', { cssClass: 'alert-danger', timeout: 3000 });

  }
  else{
    if(this.reportFromDate != undefined && this.reportToDate != undefined){

    //for from date
    const reportFromDate={
      day:this.reportFromDate["date"]["day"],
      month:this.reportFromDate["date"]["month"],
      year:this.reportFromDate["date"]["year"]
    }


    this.dateChange=this.reportFromDate["date"]["day"];
    for(var i=1;i<=9;i++){
      if(this.dateChange==i){
        let zero=0;
        let x:any;
        let y:any;

        x=zero.toString()+(this.dateChange).toString();
        this.dateChange=x;
      }
    }
    this.monthChnage=this.reportFromDate["date"]["month"];

    for(var j=1;j<=9;j++){
      if(this.monthChnage==j){
        let zero1=0;
        let m:any;

        m=zero1.toString()+(this.monthChnage).toString();

        this.monthChnage=m;
      }
    }

    this.fromDate=this.dateChange+"/"+this.monthChnage+"/"+this.reportFromDate["date"]["year"];


    //For to date
    const reportToDate={
      day:this.reportToDate["date"]["day"],
      month:this.reportToDate["date"]["month"],
      year:this.reportToDate["date"]["year"]
    }


    this.dateChange1=this.reportToDate["date"]["day"];
    for(var i=1;i<=9;i++){
      if(this.dateChange1==i){
        let zero=0;
        let x:any;
        let y:any;

        x=zero.toString()+(this.dateChange1).toString();
        this.dateChange1=x;
      }
    }
    this.monthChnage1=this.reportToDate["date"]["month"];

    for(var j=1;j<=9;j++){
      if(this.monthChnage1==j){
        let zero1=0;
        let m:any;


        m=zero1.toString()+(this.monthChnage1).toString();

        this.monthChnage1=m;
      }
    }

    //this.fromDate=this.dateChange+"/"+this.monthChnage+"/"+this.reportToDate["date"]["year"];

    this.toDate=this.dateChange1+"/"+this.monthChnage1+"/"+this.reportToDate["date"]["year"];


    if(this.apmcSel.length == 0){
      if(this.apmcListData[0] != undefined){
        this.apmcSel = this.apmcListData[0];
      }
    }

    const dateJsonTrader={
      "dateVO":{
          fromDate:this.fromDate,
          toDate:this.toDate},
      "stateVO": this.stateSel,
      "apmcVO": this.apmcSel
    }



    if(this.fileType=="pdf"){
      const lotFileType=this.fileType;
      this.authservice.getSaleAgreementPdf(dateJsonTrader,lotFileType).subscribe(
        (ret) => {
          var fileURL2 = URL.createObjectURL(ret);
          window.open(fileURL2);
        }, (err) => {
          if (err === 'Unauthorized')
          {
            this.errorPopupShow();
            setTimeout(()=>{
              this.my_class50 = 'overlay';
              localStorage.clear();
              this.router.navigateByUrl('/login');
            },3000);
          }
        }
      );
    }
    else if(this.fileType=="rtf"){
      const lotFileType=this.fileType;
      this.authservice.getSaleAgreementRtf(dateJsonTrader,lotFileType).subscribe(
        (ret) => {
          var fileURL2 = URL.createObjectURL(ret);
          window.open(fileURL2);
        }, (err) => {
          if (err === 'Unauthorized')
          {
            this.errorPopupShow();
            setTimeout(()=>{
              this.my_class50 = 'overlay';
              localStorage.clear();
              this.router.navigateByUrl('/login');
            },3000);
          }
        }
      );
    }
    else if(this.fileType=="xls"){
      const lotFileType=this.fileType;
      this.authservice.getSaleAgreementExcel(dateJsonTrader,lotFileType).subscribe(
        (ret) => {
          // var fileURL3 = URL.createObjectURL(ret);
          // window.open(fileURL3);

          let blob = ret;
          let a = document.createElement("a");
          a.href = URL.createObjectURL(blob);
          a.download = 'lotDetails.xls';
          document.body.appendChild(a);
          a.click();



        }, (err) => {
          if (err === 'Unauthorized')
          {
            this.errorPopupShow();
            setTimeout(()=>{
              this.my_class50 = 'overlay';
              localStorage.clear();
              this.router.navigateByUrl('/login');
            },3000);
          }
        }
      );
    }
    else{
      this.flashMessagesService.show('Please Choose A File Type', { cssClass: 'alert-danger', timeout: 3000 });
    }
}
    else{
      console.log("date undefined",this.reportFromDate,this.reportToDate);
      this.flashMessagesService.show('Please Enter Both Dates First', { cssClass: 'alert-danger', timeout: 3000 });

    }
  }
}


// Get Sale Bill
getSaleBill(){
  if(this.reportFromDate==undefined && this.reportToDate==undefined){
    this.flashMessagesService.show('Date is mandatory to search .', { cssClass: 'alert-danger', timeout: 3000 });

  }
  else{
    if(this.reportFromDate != undefined && this.reportToDate != undefined){

    //for from date
    const reportFromDate={
      day:this.reportFromDate["date"]["day"],
      month:this.reportFromDate["date"]["month"],
      year:this.reportFromDate["date"]["year"]
    }


    this.dateChange=this.reportFromDate["date"]["day"];
    for(var i=1;i<=9;i++){
      if(this.dateChange==i){
        let zero=0;
        let x:any;
        let y:any;

        x=zero.toString()+(this.dateChange).toString();
        this.dateChange=x;
      }
    }
    this.monthChnage=this.reportFromDate["date"]["month"];

    for(var j=1;j<=9;j++){
      if(this.monthChnage==j){
        let zero1=0;
        let m:any;

        m=zero1.toString()+(this.monthChnage).toString();

        this.monthChnage=m;
      }
    }

    this.fromDate=this.dateChange+"/"+this.monthChnage+"/"+this.reportFromDate["date"]["year"];


    //For to date
    const reportToDate={
      day:this.reportToDate["date"]["day"],
      month:this.reportToDate["date"]["month"],
      year:this.reportToDate["date"]["year"]
    }


    this.dateChange1=this.reportToDate["date"]["day"];
    for(var i=1;i<=9;i++){
      if(this.dateChange1==i){
        let zero=0;
        let x:any;
        let y:any;

        x=zero.toString()+(this.dateChange1).toString();
        this.dateChange1=x;
      }
    }
    this.monthChnage1=this.reportToDate["date"]["month"];

    for(var j=1;j<=9;j++){
      if(this.monthChnage1==j){
        let zero1=0;
        let m:any;


        m=zero1.toString()+(this.monthChnage1).toString();

        this.monthChnage1=m;
      }
    }

    //this.fromDate=this.dateChange+"/"+this.monthChnage+"/"+this.reportToDate["date"]["year"];

    this.toDate=this.dateChange1+"/"+this.monthChnage1+"/"+this.reportToDate["date"]["year"];

    if(this.apmcSel.length == 0){
      if(this.apmcListData[0] != undefined){
        this.apmcSel = this.apmcListData[0];
      }
    }

    const dateJsonTrader={
      "dateVO":{
          fromDate:this.fromDate,
          toDate:this.toDate},
      "stateVO": this.stateSel,
      "apmcVO": this.apmcSel
    }

    if(this.fileType=="pdf"){
      const lotFileType=this.fileType;
      this.authservice.getSaleBillPdf(dateJsonTrader,lotFileType).subscribe(
        (ret) => {
          var fileURL2 = URL.createObjectURL(ret);
          window.open(fileURL2);
        }, (err) => {
          if (err === 'Unauthorized')
          {
            this.errorPopupShow();
            setTimeout(()=>{
              this.my_class50 = 'overlay';
              localStorage.clear();
              this.router.navigateByUrl('/login');
            },3000);
          }
        }
      );
    }
    else if(this.fileType=="rtf"){
      const lotFileType=this.fileType;
      this.authservice.getSaleBillRtf(dateJsonTrader,lotFileType).subscribe(
        (ret) => {
          var fileURL2 = URL.createObjectURL(ret);
          window.open(fileURL2);
        }, (err) => {
          if (err === 'Unauthorized')
          {
            this.errorPopupShow();
            setTimeout(()=>{
              this.my_class50 = 'overlay';
              localStorage.clear();
              this.router.navigateByUrl('/login');
            },3000);
          }
        }
      );
    }
    else if(this.fileType=="xls"){
      const lotFileType=this.fileType;
      this.authservice.getSaleBillExcel(dateJsonTrader,lotFileType).subscribe(
        (ret) => {
          // var fileURL3 = URL.createObjectURL(ret);
          // window.open(fileURL3);

          let blob = ret;
          let a = document.createElement("a");
          a.href = URL.createObjectURL(blob);
          a.download = 'lotDetails.xls';
          document.body.appendChild(a);
          a.click();



        }, (err) => {
          if (err === 'Unauthorized')
          {
            this.errorPopupShow();
            setTimeout(()=>{
              this.my_class50 = 'overlay';
              localStorage.clear();
              this.router.navigateByUrl('/login');
            },3000);
          }
        }
      );
    }
    else{
      this.flashMessagesService.show('Please Choose A File Type', { cssClass: 'alert-danger', timeout: 3000 });
    }
}
    else{
      console.log("date undefined",this.reportFromDate,this.reportToDate);
      this.flashMessagesService.show('Please Enter Both Dates First', { cssClass: 'alert-danger', timeout: 3000 });

    }
  }
}


// Fetch all states here
fetchStatelist(stateID){
    this.authservice.getStateList().subscribe($data=>{
      this.stateListData = $data.listData;
      console.log("apmc",this.apmcSel)
      if($data.listData.length != 0){
        for(var i=0;i<$data.listData.length;i++){
          if($data.listData[i].adoumOprID==stateID){
            this.selectedState = true;
            this.stateSel=$data.listData[i];
            this.stateID = $data.listData[i].adoumOprID;
            console.log("here",this.stateSel);
          }
        }
          this.fetchApmcList(stateID );
      }
    }
    , (err) => {
          if (err === 'Unauthorized')
          {
            this.errorPopupShow();
                  setTimeout(()=>{
                    localStorage.clear();
                    this.router.navigateByUrl('/login');
                  },3000);  }
    })
}


// Fetch all apmcs here
fetchApmcList(stateID){
  this.authservice.getAPMCList(stateID).subscribe($data=>{
    // console.log($data.listData);
    this.apmcListData = $data.listData;
    if($data.listData.length != 0){
      for(var i=0;i<$data.listData.length;i++){
        if($data.listData[i].adoumOprID==this.apmcID){
          this.apmcSel=$data.listData[i];
          console.log("here",this.apmcSel);
        }
      }
    }
  }
  , (err) => {
        if (err === 'Unauthorized')
        {
            this.errorPopupShow();
                setTimeout(()=>{
                  localStorage.clear();
                  this.router.navigateByUrl('/login');
                },3000);
        }
          })
}
// select state from dropdown
chooseState(stateID){
console.log("state id",stateID);
this.apmcListData = [];
this.stateListData = [];
this.apmcSel = [];
this.fetchStatelist(stateID);
console.log("apmcSel id",this.apmcSel);

}

chooseApmc(apmcID){
  console.log("apmcSel id",this.apmcListData);
  if(this.apmcListData.length != 0){
    for(var i=0;i<this.apmcListData.length;i++){
      if(this.apmcListData[i].adoumOprID==apmcID){
        this.apmcSel=this.apmcListData[i];
      }
    }
  }
  console.log("here",this.apmcSel);

}

}
