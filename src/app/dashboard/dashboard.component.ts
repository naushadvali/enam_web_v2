import { Component, ViewChild, ElementRef, AfterViewInit,OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers } from '@angular/http';
import { AuthService } from '../services/auth.service';
import {DataTableModule} from "angular2-datatable";
import { DatePipe } from '@angular/common';
import {IMyDpOptions} from 'mydatepicker';
import { FlashMessagesService } from 'angular2-flash-messages';
import * as _ from 'lodash';
import { EnvironmentSpecificService } from '../services/environment-specific.service';


@Component({
  selector: 'dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.css'],

})
export class DashboardComponent implements OnInit{


  data=[];
  pendingdata=[];
  commodityData=[];
  outcryData=[];
  securityDepositData:any=[];
  tradeViewData=[];
  permitViewData=[];
  dateDashboard:String;
  lotcode:String;
  invoiceno:String;
  dateInsert:any;
  pendingListData=[];
  permitReqData:any;
  IMyDpOptions:any;
  pieChartLabels:string[];
  pieChartData:number[];
  pieChartType:string;
  message: string;
  issued:String;
  requested:String;
  amount:any;
  payamount:string;
  fromDate:string;
  x:number;
  y:number;
  totalPending:number;
  public filteredData;

  auchAmtToPay:any;
  agentName:any;
  auchAgenID:any;
  merchant_param3:any;
  auchInvoiceDate:any;
  merchant_param5:any;
  merchant_param2:any;
  auchTranID:any;
  farmerName:any;
  lotCode:any;
  paymentAllowed:any;
  status1:string;
  status2:string;
  billing_address:any;
  billing_tel:any;
  billing_email:any;
  billing_zip:any;
  lcamTrnID:any;
  billing_name:any;
  merchant_param1:any;
  merchant_id:any = "95670";
  merchant_param4:any;
  chln_opr_id:any;
  client_code:any;
  tid:any;//today time in milliseconds
  currency:any = "INR";
  language = "EN";
  FinalURL:any="http://staging.techl33t.com/enam/dist";
  redirect_url:any = this.FinalURL + "/pgwresponse";
  cancel_url:any = this.FinalURL + "/pgwresponse";
  ccaRequest:string;
  order_id:any;
  billing_country:any="India";
  encRequest:any;
  hideElement:boolean;
  dateChange:any;
  monthChnage:any;
  public my_class1='overlay';
  public my_class2='overlay';
  public my_class4='overlay';

  //LIVE STAGING
   accessCode:any = "AVCP00DK77BL85PCLB";
   workingKey:any ="9B939267262A83C4D8B17C1C4C32DF16";
   checkRadio :boolean;

  // 192.168.0.85
  // accessCode:any = "AVSC00DD38AT60CSTA"; //Put in the Access Code in quotes provided by CCAVENUES.
  // workingKey:any = "23FD883A05218345F0F8060E24A9CB5E"; //Put in the 32 Bit Working Key provided by CCAVENUES.

  //27.251.120.242
  // accessCode:any = "AVSC00DD38AT61CSTA"; //Put in the Access Code in quotes provided by CCAVENUES.
  // workingKey:any ="831F0AD1BB89004442CCD2D0065D754A"; //Put in the 32 Bit Working Key provided by CCAVENUES.

link1: string;
  constructor(public authservice:AuthService,
    private _dataTable:DataTableModule,
    private flashMessagesService: FlashMessagesService,
    private http:Http,private router:Router,
    private envSpecificSvc: EnvironmentSpecificService
  )
  {

    //this.pieChartData = [parseInt(this.tradeViewData["status"]),parseInt(this.tradeViewData[1].status)];
    //this.pieChartType= 'pie';
     this.pieChartLabels = ["Cancelled","Agreement Done"];
     this.pieChartData = [this.x,this.y];
     this.pieChartType= 'pie';
    this.payDisabled = true;
    this.checkRadio = false;
  }

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    //dateFormat: 'dd/mm/yyyy',
    dateFormat: 'dd-mm-yyyy',
    editableDateField:false,
    openSelectorOnInputClick:true,
    inline:false
  };

  ngOnInit() {

    //TRADE VIEW
    this.permitReqData={};
    // this.link1 = this.envSpecificSvc.envSpecific.loginUrl;
    // console.log("login url",this.link1);

    //COMMODITY LIST
    this.authservice.getCommodityList().subscribe
    (
      data=>{
        let $data=data;
        this.commodityData=$data.listData;
        //console.log("commodityData comes here: ",$data);
        //alert(this.data);
      }, (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
        }
      }

    );

    //OUTCRY
    this.authservice.getOutcry().subscribe
    (
      data=>{
        let $data=data;
        this.outcryData=$data.listData;
        //console.log("outcryData comes here: ",this.outcryData);
      }

, (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
        }
      }
    );

    //SECURITY DEPOSIT
    this.authservice.getSecurityDeposit().subscribe
    (
      $data=>{

        this.securityDepositData=$data.data;
        //console.log("securityDepositData comes here: ",this.securityDepositData);
      }

      , (err) => {
                  if (err === 'Unauthorized')
                  {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
              }
            }
    );

    this.authservice.getTradeView().subscribe
    (
      data=>{
        let $data=data;
        this.tradeViewData=$data.listData;
        //console.log("tradeViewData comes here: ",this.tradeViewData);
        this.status1=this.tradeViewData[0].status;
        this.status2=this.tradeViewData[1].status;
        this.x=this.tradeViewData[0].cntAgree;
        this.y=this.tradeViewData[1].cntAgree;
        //var  pies = [];
        //pies.push(this.tradeViewData);
          this.pieChartLabels = ["Cancelled","Agreement Done"];
          //console.log(pies);
        //this.pieChartLabels = ["Cancelled","Agreements Done"];
       //this.pieChartData = [(parseInt(pies[0][0]["status"])),(parseInt(pies[0][1]["status"]))];
        this.pieChartData = [this.x,this.y];
        this.pieChartType= 'pie';

      }

, (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
        }
      }
    );
  }

  doSearch(){
    console.log("dateInsert in undefined",this.dateInsert);
    if(this.dateInsert==undefined){
      this.flashMessagesService.show('Date is mandatory to search. LotCode and Invoice No. is optional .', { cssClass: 'alert-danger', timeout: 3000 });
    }

    else if(this.dateInsert==null){
        this.flashMessagesService.show('Date is in incorrect format .', { cssClass: 'alert-danger', timeout: 3000 });
      }


    else{
      //console.log("dateInsert",this.dateInsert["date"]["day"]);
      //console.log("dateInsert",this.dateInsert["date"]["month"]);
      //console.log("dateInsert",this.dateInsert["date"]["year"]);
      //console.log("dateInsert",this.dateInsert.formatted);
      //console.log("lotcode",this.lotcode);
      //console.log("invoiceno",this.invoiceno);
      //console.log("invoiceno",this.invoiceno);
      console.log("else dateInsert in undefined",this.dateInsert);
      const dateInsert={
        day:this.dateInsert["date"]["day"],
        month:this.dateInsert["date"]["month"],
        year:this.dateInsert["date"]["year"]
      }
      if(this.lotcode==undefined || this.lotcode==''){
        this.lotcode=null;
      }
      const lotcode=this.lotcode;
      if(this.invoiceno==undefined || this.invoiceno==''){
        this.invoiceno=null;
      }
      const invoiceno=this.invoiceno;
	  this.dateChange=this.dateInsert["date"]["day"];
	  for(var i=1;i<=9;i++){
		  if(this.dateChange==i){
			  let zero=0;
			  let x:any;
			  let y:any;

			  x=zero.toString()+(this.dateChange).toString();
        this.dateChange=x;
		  }
	  }
	  this.monthChnage=this.dateInsert["date"]["month"];
	  //console.log("this.monthChnage is:",this.monthChnage);
	  for(var j=1;j<=9;j++){
		  if(this.monthChnage==j){
			  let zero1=0;
			  let m:any;

			  //console.log("changed date: ",zero1,this.monthChnage);
			  m=zero1.toString()+(this.monthChnage).toString();
			  //console.log("m is:",m);
			  this.monthChnage=m;
		  }
	  }
	  //console.log("Date change is: ",this.dateChange);
      this.fromDate=this.dateChange+"/"+this.monthChnage+"/"+this.dateInsert["date"]["year"];
      //console.log("from date from Dashboard: ",this.fromDate);
      const fromDate=this.fromDate;
      this.authservice.getPendingList(fromDate,lotcode,invoiceno).subscribe
      (
        data=>{
          let $data=data;
          this.pendingdata=$data.listData;
          this.filteredData=this.pendingdata;
          console.log("filtered DATA: ",$data.listData);
          if(this.filteredData.length==0){
              this.flashMessagesService.show('No records found ', { cssClass: 'alert-info', timeout: 3000 });
              this.totalPending=0;
          }else{
            //console.log("Pending Data comes here: ",this.pendingdata);

            let sum=0;
            for(let j=0;j<$data.listData.length;j++){
              //console.log("pending amount",parseInt(JSON.parse($data.listData[j].auchAmtToPay)));
              sum=sum+parseInt(JSON.parse($data.listData[j].auchAmtToPay));
              //console.log("Total pending amount ",sum);
            }
            this.totalPending=sum;
          }

        }

        , (err) => {
                    if (err === 'Unauthorized')
                    {
                      this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
                }
              }
      );
    }



  }
  //On click of Pay button
  collectedData:any;
  collectData(post:any){
    console.log("inside collect data",post);
    this.amount=post.auchAmtToPay;
    this.agentName=post.agentName;
    this.auchAgenID=post.auchAgenID;
    this.merchant_param3=post.auchInvoiceDOCNO;
    this.auchInvoiceDate=post.auchInvoiceDate;
    this.merchant_param5=post.auchOprID;
    this.merchant_param2=post.auchOrgID;
    this.auchTranID=post.auchTranID;
    this.farmerName=post.farmerName;
    this.lotCode=post.lotCode;
    this.paymentAllowed=post.paymentAllowed;
    this.tid=Date.now();
    this.merchant_param4=localStorage.getItem('userID');
    this.chln_opr_id=post.auchOprID;
    this.client_code="NAM"+parseInt(post.auchOprID);
    this.merchant_param1=this.client_code;
    //console.log("selected row: ",this.tid);
    this.my_class1='overlay1';
    this. collectedData = post;
  }

  payNow(){

      console.log("collectData",this.collectedData)
      this.authservice.postPayment(this.collectedData).subscribe($data=>{


        if($data.status==1){
          console.log("response data: ",$data);
          this.accessCode=$data.data.accessKey;
          this.encRequest=$data.data.encrypted;
    
          // this.billing_address=$data.data.lcamAgentAddress;
          // this.billing_tel=$data.data.lcamAgentContactNo;
          // this.billing_email=$data.data.lcamAgentEmailID;
          // this.billing_zip=$data.data.lcamAgentPincode;
          // this.lcamTrnID=$data.data.lcamTrnID;
          // this.billing_name=$data.data.lcamFullName;
          // this.order_id="NULL";
          // //console.log("Customer respective data:  ",this.lcamAgentContactNo);
          // this.ccaRequest =
          //            "tid=" + this.tid + "&merchant_id=" + this.merchant_id + "&order_id=" + this.order_id + "&currency=" + this.currency +
          //            "&amount=" + this.amount + "&redirect_url=" + this.redirect_url + "&cancel_url=" + this.cancel_url + "&language=" +
          //            this.language + "&merchant_param1=" + this.merchant_param1 + "&merchant_param5=" + this.merchant_param5 + "&merchant_param2=" + this.merchant_param2 + "&merchant_param3=" + this.merchant_param3 +
          //            "&merchant_param4=" + this.merchant_param4 + "&billing_name=" + this.billing_name + "&billing_address=" +
          //            this.billing_address + "&billing_zip=" + this.billing_zip + "&billing_tel=" + this.billing_tel +
          //            "&billing_email=" + this.billing_email + "&billing_country="+this.billing_country;

                     //console.log("Inside paynow");
                       const ccaRequest={ccaRequest:$data.encrypted};



//                        this.authservice.getCcaRequest(ccaRequest).subscribe($data=>{
//                          //console.log("Response CCAREQUEST from server: ",$data);
//                          if($data.status==1){
//                            this.accessCode=$data.data.accessCode;
//                            this.encRequest=$data.data.ccaRequest;
//                            //console.log("Getting ready to send to ccavenue: ",this.accessCode);
//                          }
//                        }
// , (err) => {
//             if (err === 'Unauthorized')
//             {
//               this.errorPopupShow();
//                     setTimeout(()=>{
//                       this.my_class4 = 'overlay';
//                       localStorage.clear();
//                       this.router.navigateByUrl('/login');
//                     },3000);
//         }
//       }
//
//                      )
                     }

        // else{
        //   console.log("Error Customer details not coming.");
        // }

      }

      , (err) => {
                  if (err === 'Unauthorized')
                  {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
              }
            }
    );

  }

payShow1(){
  this.my_class2='overlay1';
}
payHide(){
  this.payDisabled = true;
  if(this.checkRadio == undefined || this.checkRadio == true  ){
    this.checkRadio = false;
  }
  this.my_class1='overlay';
}
payDisabled:boolean;

openPayNow(value){
  console.log("value",value,this.checkRadio);
  if(value == false || value == undefined){
    this.payDisabled = true;
  }
  if(value == true){
      this.payDisabled = false;
  }
}
payHide1(){
  this.my_class2='overlay';
}
errorPopupShow(){
  this.my_class4='overlay1';
}
errorPopupHide(){
  this.my_class4='overlay';
}


  searchLotcode(query:string){
    //console.log("lotcode search",query);
    //this.filteredData=this.data;
    if(query) {
      this.filteredData = _.filter(this.pendingdata, (a)=>a.lotCode.indexOf(query)>=0);
    } else {
      this.filteredData = this.pendingdata  ;
    }

  }
  searchInvoiceNo(query:string){
    //console.log("Invoice no search",query);
    //this.filteredData=this.data;
    if(query) {
      this.filteredData = _.filter(this.pendingdata, (a)=>a.auchInvoiceDOCNO.indexOf(query)>=0);
    } else {
      this.filteredData = this.pendingdata  ;
    }

  }

  toggle : boolean = true;
  getPermitReq(){
      //this.errorPopupShow();
      // Workign toggle
           if (this.hideElement!=true) {
              this.hideElement=true;
              //console.log("css check ",this.hideElement);
            }
          else {
               this.hideElement=false;
                //console.log("css check ",this.hideElement);
           }

    //PERMIT REQUEST
    this.authservice.getPermitReq().subscribe
    (
      data=>{
        let $data=data;
        this.permitReqData=$data.data;
        //console.log("permtreq comes here: ",this.permitReqData);
        //alert(this.data);
      }

      , (err) => {
                  if (err === 'Unauthorized')
                  {
                    this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
                    // localStorage.clear();
                    // this.router.navigateByUrl('/login');
              }
            }
    );
  }

  getPdf(post){
    console.log("collectData",post)

    this.authservice.getChallanPdf(post).subscribe(
      (res) => {
        var fileURL = URL.createObjectURL(res);
        window.open(fileURL);
        }
        , (err) => {
                    if (err === 'Unauthorized')
                    {
                      this.errorPopupShow();
                            setTimeout(()=>{
                              this.my_class4 = 'overlay';
                              localStorage.clear();
                              this.router.navigateByUrl('/login');
                            },3000);
                }
              }
    )
  }

  //on Click Search Button

// events
  public chartClicked(e:any):void {
    //console.log(e);
  }

  public chartHovered(e:any):void {
    //console.log(e);
  }
  //PERMITS POPUP PRINT
  print(){
    this.authservice.getChallanPdf(this.collectedData).subscribe(
      (res) => {
        var fileURL = URL.createObjectURL(res);
        window.open(fileURL);
        }
, (err) => {
            if (err === 'Unauthorized')
            {
              // localStorage.clear();
              // this.router.navigateByUrl('/login');
              this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
        }
      }
    )
  }
  //Table sorting

}
