import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommoditybidlistingComponent } from './commoditybidlisting.component';

describe('CommoditybidlistingComponent', () => {
  let component: CommoditybidlistingComponent;
  let fixture: ComponentFixture<CommoditybidlistingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommoditybidlistingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommoditybidlistingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
