import { Component,Input, OnInit, ElementRef, OnDestroy  } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { AuthService } from '../services/auth.service';
import { FormsModule, ReactiveFormsModule,FormBuilder, Validators ,NG_VALIDATORS} from '@angular/forms';
import { Observable, Subscription } from 'rxjs/Rx';
import { FlashMessagesService } from 'angular2-flash-messages';
import { DatePipe } from '@angular/common';
import { TimerComponent } from '../timer/timer.component';
import * as moment from 'moment';
import * as _ from 'lodash';
import { Router } from '@angular/router';
@Component({
  selector: 'app-commoditybidlisting',
  templateUrl: './commoditybidlisting.component.html',
  styleUrls: ['./commoditybidlisting.component.css'],
  providers: [DatePipe]
})
export class CommoditybidlistingComponent implements OnInit {

  data:any=[];
  commodityBidData:any=[];
  imgsrc=[]; //"assets/images/stargrey.png";
  remaintime:string;
  adoumOprName:string;
  gmpmgProdName:string;
  iledLotCode:string;
  abcOrgID:string;
  iledNoofBag:string;
  iledQTY:string;
  abcMspBidRate:string;
  maxOpenBidValue:string;
  abcStartDate:string;
  abcEndDate:string;
  abcExtendedEndTime:string;
  seller:string;
  remainingTime:string;
  prodName:string;
  CA:string;
  abcAllowPartial:string;
  abcAutoAssinWiner:string;
  abcBidID:string;
  abcBidOpenDate:string;
  abcBidOpenTime:string;
  abcBidStatus:string;
  abcBidType:string;
  abcLotID:string;
  abcMinBuyers:string;
  abcOpenBidNextDay:string;
  abcOprID:string;
  abcStartTime:string;
  abcSubMultBid:string;
  abcTranID:string;
  abcTrnDate:string;
  createdBy:string;
  createdOn:string;
  gmpmgProdCode:string;
  gmpmgProdID:string;
  gmptmDESC:string;
  iledLotDate:string;
  lastBidValue:string;
  wbWeight:string;
  replyServer:any;
  disableConfirm:boolean;
  uniqueProdName:any=[];
  resultUniq:any=[];
  commoditybidlistingError:any;
  addpreferError:any;
  certificateData=[];
  public my_class='overlay';
  public my_class4:any='overlay';
  public my_class6:any='overlay';


  public filteredData=[];
  private alive: boolean;
  private timer: Observable<number>;
  private interval: number;


  ngOnInit() {
    this.disableConfirm=true;
    this.selProduct = "all";
    // this.getCommodityList();
    this.pageNo = 0;
    this.getAllCommodityBidListing();
    this.getCommodityNewBidListing();
  }

  chngImg(post:any,index){
    //console.log("change image",post,index);
    //console.log("Data in index commodity",index);
    var a=document.getElementById("cdata_"+index);


    if(post.imgsrc=="assets/images/stargrey.png"){
      this.filteredData.forEach(function(obj) {
        if(obj.abcLotID == post.abcLotID){
          post.imgsrc="assets/images/star.png";
        }
        else{
          obj.imgsrc="assets/images/stargrey.png";
        }
        // obj.imgsrc = "assets/images/stargrey.png";

      });
      // post.imgsrc="assets/images/star.png";
      //console.log("these are post from chngimg",post.adoumOprName);
      this.adoumOprName=post.adoumOprName;
      this.gmpmgProdName=post.gmpmgProdName;
      this.iledLotCode=post.iledLotCode;
      this.abcOrgID=post.abcOrgID;
      this.iledNoofBag=post.iledNoofBag;
      this.iledQTY=post.iledQTY;
      this.abcMspBidRate=post.abcMspBidRate;
      this.maxOpenBidValue=post.maxOpenBidValue;
      this.abcStartDate=post.abcStartDate;
      this.abcEndDate=post.abcEndDate;
      this.abcExtendedEndTime=post.abcExtendedEndTime;
      this.seller=post.seller;
      this.remainingTime=post.remainingTime;

      this.CA=post.CA;
      this.abcAllowPartial=post.abcAllowPartial;
      this.abcAutoAssinWiner=post.abcAutoAssinWiner;
      this.abcBidID=post.abcBidID;
      this.abcBidOpenDate=post.abcBidOpenDate;
      this.abcBidOpenTime=post.abcBidOpenTime;
      this.abcBidStatus=post.abcBidStatus;
      this.abcBidType=post.abcBidType;
      this.abcLotID=post.abcLotID;
      this.abcMinBuyers=post.abcMinBuyers;
      this.abcOpenBidNextDay=post.abcOpenBidNextDay;
      this.abcOprID=post.abcOprID;
      this.abcStartTime=post.abcStartTime;
      this.abcSubMultBid=post.abcSubMultBid;
      this.abcTranID=post.abcTranID;
      this.abcTrnDate=post.abcTrnDate;
      this.createdBy=post.createdBy;
      this.createdOn=post.createdOn;
      this.gmpmgProdCode=post.gmpmgProdCode;
      this.gmpmgProdID=post.gmpmgProdID;
      this.gmptmDESC=post.gmptmDESC;
      this.iledLotDate=post.iledLotDate;
      this.lastBidValue=post.lastBidValue;
      this.wbWeight=post.wbWeight;
      this.disableConfirm=false;
    }else{
      post.imgsrc="assets/images/stargrey.png";
      this.disableConfirm=true;
    }
    console.log("change image",post);

  }
  constructor(
    private _dataTable:DataTableModule,
    private authservice:AuthService,
    private datepipe:DatePipe,
    private flashMessagesService: FlashMessagesService,
    private router:Router,
  ) {

    this.alive = true;
    this.interval = 120000;
    this.timer = Observable.timer(0, this.interval);
    // this.data=this.authservice.getReport();
    // //console.log("Rebid allowed data",this.data);
    //   for(let i=0;i<this.data.length;i++)
    //     {
    //       //console.log("these: ",this.data[i]);
    //     }
    this.commodityBidData.forEach(function(obj) {
      obj.imgsrc = "assets/images/stargrey.png";

    });
    //console.log("commodityBidData comes here: ",this.data);
    //COMMODITY BID DATA
    this.timer
    .takeWhile(() => this.alive)
    .subscribe(() =>{
      this.commoditybidlistingError=localStorage.getItem('commodityBidListingError');
      if(this.commoditybidlistingError==0){
       this.flashMessagesService.show('Connection error', { cssClass: 'alert-danger', timeout: 3000 });
     }


    })

  }
  viewCertificate(){
    const abcOprID=this.abcOprID;
    const abcLotID=this.abcLotID;
    // console.log("abcOprID is",this.abcOprID);
    // console.log("abcLotID is",this.abcLotID);
    this.authservice.getCertificate(abcOprID,abcLotID).subscribe($data=>{
      this.certificateData=$data.listData;
      // console.log("Certificate data comes here",this.certificateData);
    }, (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
        }
      }

  );
    this.my_class='overlay1';
  }

  seeTradeResult(){
    const abcOprID=this.abcOprID;
    const abcLotID=this.abcLotID;
    this.authservice.getTradeResult(abcLotID,abcOprID).subscribe(
      (res) => {
        var fileURL = URL.createObjectURL(res);
        window.open(fileURL);
        }
        , (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
        }
      }

    )
  }



  offComCertifiacate(){
    this.my_class='overlay';
  }

  selProduct:string;
  onProdSelection(val){
    //console.log(val);
    this.selProduct = val;
    // this.prodName=val;
    // if(val) {
    //   this.filteredData = _.filter(this.commodityBidData, (a)=>a.gmpmgProdName.indexOf(val)>=0);
    //   //console.log(this.filteredData);
    // }
    // if(this.prodName==="All") {
    //   this.filteredData = this.commodityBidData  ;
    // }
  }


  test(value, index){
    //console.log("REC", value);
    //console.log("REC ind", index);
    if(value <= 0){
      this.commodityBidData[index].abcSubMultBid = "N"
    }
    //console.log("OBJ",this.mainContent[index].abcSubMultBid = "N")
  }
  //Button BidNow Validation check
  chkBid(post:any){
    //console.log("chkbid: ",post.abcSubMultBid);
    if(post.abcSubMultBid=="Y"){
      //console.log("yehh!!!");

      return true;
    }else{
      //console.log("Sorry");
      return false;
    }
  }
  errorPopupShow(){
  this.my_class4='overlay1';
}

  addPrefer(){
    //console.log("this is on buuton click showing OPRname  ",this.adoumOprName);
    const addPrefered={
      adoumOprName:this.adoumOprName,
      gmpmgProdName:this.gmpmgProdName,
      iledLotCode:this.iledLotCode,
      abcOrgID:this.abcOrgID,
      iledNoofBag:this.iledNoofBag,
      iledQTY:this.iledQTY,
      abcMspBidRate:this.abcMspBidRate,
      maxOpenBidValue:this.maxOpenBidValue,
      abcStartDate:this.abcStartDate,
      abcEndDate:this.abcEndDate,
      abcExtendedEndTime:this.abcExtendedEndTime,
      seller:this.seller,
      remainingTime:this.remainingTime,

      CA:this.CA,
      abcAllowPartial:this.abcAllowPartial,
      abcAutoAssinWiner:this.abcAutoAssinWiner,
      abcBidID:this.abcBidID,
      abcBidOpenDate:this.abcBidOpenDate,
      abcBidOpenTime:this.abcBidOpenTime,
      abcBidStatus:this.abcBidStatus,
      abcBidType:this.abcBidType,
      abcLotID:this.abcLotID,
      abcMinBuyers:this.abcMinBuyers,
      abcOpenBidNextDay:this.abcOpenBidNextDay,
      abcOprID:this.abcOprID,
      abcStartTime:this.abcStartTime,
      abcSubMultBid:this.abcSubMultBid,
      abcTranID:this.abcTranID,
      abcTrnDate:this.abcTrnDate,
      createdBy:this.createdBy,
      createdOn:this.createdOn,
      gmpmgProdCode:this.gmpmgProdCode,
      gmpmgProdID:this.gmpmgProdID,
      gmptmDESC:this.gmptmDESC,
      iledLotDate:this.iledLotDate,
      lastBidValue:this.lastBidValue,
      wbWeight:this.wbWeight,
    }
    // this.addpreferError=sessionStorage.getItem('addPreferredError');
    // if(this.addpreferError==0){
    //     this.flashMessagesService.show('Connection error', { cssClass: 'alert-danger', timeout: 3000 });
    //   }
    this.authservice.addPreferredData(addPrefered).subscribe($data=>{
      //console.log("After calling addprefred button ",$data);
      this.replyServer=$data;
      if(this.replyServer.status==1){
        // this.flashMessagesService.show('successfully added', { cssClass: 'alert-success', timeout: 3000 });
          this.my_class6 = 'overlay1';
          this.commodityBidData =[];
          this.filteredData =[];
          setTimeout(()=>{
            this.my_class6 = 'overlay';
            this.ngOnInit();
          },2000);
        //console.log("Yes,status is 1.Please refresh now.");
        //window.location.reload();
      }else{
        // this.flashMessagesService.show('not added', { cssClass: 'alert-danger', timeout: 3000 });
        this.my_class6 = 'overlay1';
        setTimeout(()=>{
          this.my_class6 = 'overlay';
        },2000);
      }
    }, (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
        }
      }
),err => console.log(err);

  }
    refresh(): void {
      window.location.reload();
  }


  //get all commodity bid LISTING
  pageNo:number;
  getAllCommodityBidListing(){
  this.pageNo = this.pageNo +1;
  // var postJson = {  "commodityId":" "}
  if(this.selProduct == 'all'){
     this.selProduct = undefined;
  }
  if(this.selProduct == ''){
     this.selProduct = undefined;
  }

    var postJson = {"commodityId": this.selProduct};
    this.authservice.getCommodityBidListingPagination(this.pageNo,postJson).subscribe
    (
      data=>{
        let $data=data;
        let adoumOprName:string;
        let gmpmgProdName:string;
        let iledLotCode:string;
        let abcOrgID:string;
        let iledNoofBag:string;
        let iledQTY:string;
        let abcMspBidRate:string;
        let maxOpenBidValue:string;
        let abcStartDate:string;
        let abcEndDate:string;
        let abcExtendedEndTime:string;
        let seller:string;
        let remainingTime:string


        // this.commodityBidData=$data.listData;
        // this.filteredData=this.commodityBidData;
        $data.listData.forEach(item => {
          item.imgsrc = "assets/images/stargrey.png";
          this.filteredData.push(item),this.commodityBidData.push(item)
          });
        for(let i=0;i<this.commodityBidData.length;i++){
          //console.log("This is End date to Parse: ",this.commodityBidData[i].abcEndDate);

          this.abcOprID=this.commodityBidData[i].abcOprID;
          this.abcLotID=this.commodityBidData[i].abcLotID;
          adoumOprName=this.commodityBidData[i].adoumOprName;
          gmpmgProdName=this.commodityBidData[i].gmpmgProdName;
          this.uniqueProdName.push(this.commodityBidData[i].gmpmgProdName);
          iledLotCode=this.commodityBidData[i].iledLotCode;
          abcOrgID=this.commodityBidData[i].abcOrgID;
          iledNoofBag=this.commodityBidData[i].iledNoofBag;
          iledQTY=this.commodityBidData[i].iledQTY;
          abcMspBidRate=this.commodityBidData[i].abcMspBidRate;
          maxOpenBidValue=this.commodityBidData[i].maxOpenBidValue;
          abcStartDate=this.commodityBidData[i].abcStartDate;
          abcEndDate=this.commodityBidData[i].abcEndDate;
          abcExtendedEndTime=this.commodityBidData[i].abcExtendedEndTime;
          seller=this.commodityBidData[i].seller;
          remainingTime=this.commodityBidData[i].remainingTime;
          // Parsing Data UTC format to Known Format 15.05.2017 15:55:55
          // let parseDate=this.commodityBidData[i].abcEndDate;
          // let x=moment.utc('2017-09-21T11:34:40Z').format('YYYY-MM-DD HH:mm:ss');
          // console.log("Parsed Date:  ",x);
          // let endTime="15:00:00";
        }
          this.resultUniq=this.uniqueProdName.filter((x, i, a) => x && a.indexOf(x) === i);
        //console.log("Rebid allowed data",data);
        //this.commodityBidData.forEach(function(obj) { obj.imgsrc = "assets/images/stargrey.png"; });
        //console.log("commodityBidData comes here: ",this.commodityBidData);
        let test=this.datepipe.transform(Date.now(),"MMMM dd yyyy");
        // this.commodityBidData.forEach(function(obj) {
        //   obj.imgsrc = "assets/images/stargrey.png";
        //
        // });


      }, (err) => {
          if (err === 'Unauthorized')
          {
            this.errorPopupShow();
                  setTimeout(()=>{
                    this.my_class4 = 'overlay';
                    localStorage.clear();
                    this.router.navigateByUrl('/login');
                  },3000);
      }
    }

    )
  }


  //sort by field
  sortapmcASC:boolean = true;
  sortBagNoASC:boolean = true;
  sortQuantityASC:boolean = true;
  sortMinBidASC:boolean = true;
  sortLastBidASC:boolean = true;
  sortTable(sort_by){
      if(sort_by == 'apmc'){
        this.sortapmcASC = !this.sortapmcASC;
          if(this.sortapmcASC){
            this.filteredData.sort( function(name1, name2) {
            if ( name1.adoumOprName < name2.adoumOprName ){
              return -1;
            }else if( name1.adoumOprName > name2.adoumOprName ){
                return 1;
            }else{
              return 0;
            }
          });
        }
        else{
          this.filteredData.sort( function(name1, name2) {
          if ( name1.adoumOprName > name2.adoumOprName ){
            return -1;
          }else if( name1.adoumOprName < name2.adoumOprName ){
              return 1;
          }else{
            return 0;
          }
        });
        }

      }
      if(sort_by == 'bagNo'){
        this.sortBagNoASC = !this.sortBagNoASC;
          if(this.sortBagNoASC){
            this.filteredData.sort( function(name1, name2) {
            if ( name1.iledNoofBag < name2.iledNoofBag ){
              return -1;
            }else if( name1.iledNoofBag > name2.iledNoofBag ){
                return 1;
            }else{
              return 0;
            }
          });
        }
        else{
          this.filteredData.sort( function(name1, name2) {
          if ( name1.iledNoofBag > name2.iledNoofBag ){
            return -1;
          }else if( name1.iledNoofBag < name2.iledNoofBag ){
              return 1;
          }else{
            return 0;
          }
        });
        }
      }
      if(sort_by == 'quantityInKg'){
        this.sortQuantityASC = !this.sortQuantityASC;
          if(this.sortQuantityASC){
            this.filteredData.sort( function(name1, name2) {
            if ( name1.iledQTY < name2.iledQTY ){
              return -1;
            }else if( name1.iledQTY > name2.iledQTY ){
                return 1;
            }else{
              return 0;
            }
          });
        }
        else{
          this.filteredData.sort( function(name1, name2) {
          if ( name1.iledQTY > name2.iledQTY ){
            return -1;
          }else if( name1.iledQTY < name2.iledQTY ){
              return 1;
          }else{
            return 0;
          }
        });
        }
      }
      if(sort_by == 'minBid'){
        this.sortMinBidASC = !this.sortMinBidASC;
          if(this.sortMinBidASC){
            this.filteredData.sort( function(name1, name2) {
            if ( name1.abcMspBidRate < name2.abcMspBidRate ){
              return -1;
            }else if( name1.abcMspBidRate > name2.abcMspBidRate ){
                return 1;
            }else{
              return 0;
            }
          });
        }
        else{
          this.filteredData.sort( function(name1, name2) {
          if ( name1.abcMspBidRate > name2.abcMspBidRate ){
            return -1;
          }else if( name1.abcMspBidRate < name2.abcMspBidRate ){
              return 1;
          }else{
            return 0;
          }
        });
        }
      }
      if(sort_by == 'lastBid'){
        this.sortLastBidASC = !this.sortLastBidASC;
          if(this.sortLastBidASC){
            this.filteredData.sort( function(name1, name2) {
            if ( name1.maxOpenBidValue < name2.maxOpenBidValue ){
              return -1;
            }else if( name1.maxOpenBidValue > name2.maxOpenBidValue ){
                return 1;
            }else{
              return 0;
            }
          });
        }
        else{
          this.filteredData.sort( function(name1, name2) {
          if ( name1.maxOpenBidValue > name2.maxOpenBidValue ){
            return -1;
          }else if( name1.maxOpenBidValue < name2.maxOpenBidValue ){
              return 1;
          }else{
            return 0;
          }
        });
        }
      }
    }


    //filter by commodity
    searchByCommodity(){
      console.log("selProduct",this.selProduct)
      this.getAllCommodityBidList();
    }

    // get all commodity bid listing
    commodityObjArr=[];
    getAllCommodityBidList(){
      if(this.selProduct == 'all'){
         this.selProduct = undefined;
      }
      if(this.selProduct == ''){
         this.selProduct = undefined;
      }
      var  postJson = {"commodityId": this.selProduct};

      console.log("this.selProduct",this.selProduct)
      this.pageNo =0;
      this.pageNo = this.pageNo +1;
      this.authservice.getCommodityBidListingPagination(this.pageNo,postJson).subscribe(
        data=>{
          console.log("data",data)
          let listData = data.listData;
          if(this.selProduct == 'all'){
            this.filteredData = [];
            var commodityObjArr=[];
            for(var i=0;i<listData.length;i++){
              if(listData[i].gmpmgProdName != undefined){
                var commodityObj={"commodityName":listData[i].gmpmgProdName,"commodityId": listData[i].gmpmgProdID};
              }
              commodityObjArr.push(commodityObj);
            }
            this.commodityObjArr = this.removeDuplicates(commodityObjArr,"commodityName")
            this.filteredData = listData;
            this.filteredData.forEach(function(obj) {
              obj.imgsrc = "assets/images/stargrey.png";

            });
          }
          else{
            this.filteredData = [];
            this.filteredData = listData;
            this.filteredData.forEach(function(obj) {
              obj.imgsrc = "assets/images/stargrey.png";

            });
          }

          console.log("commodityObjArr",this.commodityObjArr,commodityObjArr)

        }
        , (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
        }
      }
      )
    }

    getCommodityList(){
      var postJson = {};
      console.log("this.selProduct",this.selProduct)
      this.authservice.getCommodityBidListing(postJson).subscribe(
        data=>{
          console.log("data",data)
          let listData = data.listData;
            var commodityObjArr=[];
            for(var i=0;i<listData.length;i++){
              if(listData[i].gmpmgProdName != undefined){
                var commodityObj={"commodityName":listData[i].gmpmgProdName,"commodityId": listData[i].gmpmgProdID};
              }
              commodityObjArr.push(commodityObj);
            }
            this.commodityObjArr = this.removeDuplicates(commodityObjArr,"commodityName")

            console.log("commodityObjArr",this.commodityObjArr,commodityObjArr)

        }
        , (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
        }
      }
      )
    }
    // Remove duplicates from array of objects
    removeDuplicates(myArr, prop) {
          return myArr.filter((obj, pos, arr) => {
              return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
          });
      }

    // get all  commodity bid list
    allCommodityObjArr=[];
    getCommodityNewBidListing(){
      this.authservice.getAllCommodityList().subscribe(
        data=>{
          console.log("comm new bid data",data)
          let listData = data.listData;
          if(listData != undefined){
            this.allCommodityObjArr=listData;
          }
        }
        , (err) => {
            if (err === 'Unauthorized')
            {
              this.errorPopupShow();
                    setTimeout(()=>{
                      this.my_class4 = 'overlay';
                      localStorage.clear();
                      this.router.navigateByUrl('/login');
                    },3000);
        }
      }
      )
    }

}
