import { Component ,Attribute,OnInit} from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ClockService } from '../services/clock.service';
import { Router } from '@angular/router';
import {SelectModule} from 'ng2-select';
 import { LocalStorageModule } from 'angular-2-local-storage';
import { FlashMessagesService } from 'angular2-flash-messages';
import { DOCUMENT } from '@angular/platform-browser';
import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';
import {MomentModule} from 'angular2-moment/moment.module';
import {Observable} from 'rxjs/Rx';
import { EnvironmentSpecificService } from '../services/environment-specific.service';


@Component({
  moduleId: module.id,
  selector: 'navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.css'],
})
  export class NavbarComponent implements OnInit {
  /*CurrentDate = Date.now();*/
  time: Date;
  state=[];
  items=[];
  adoumOprID:any;
  adoumOprName:StringConstructor;
  adoumOrgID:any;
  data={};
  listData=[];
  apmclocation:string;
  agent:string;
  commodity:string;
  lotcode:string;
  seller:string;
  village:string;
  location:string;
  location1:string;
  userEmailID:any;
  userName:any;
  userPhoneNo:any;
  email:string;
  phone:any;
  pwd:any;
  cpwd:any;
  locationList:any;
  typeMLocation:string;
  chkDisable:boolean=true;
  userType:string;
  emailId:any;
  logoutError:any;
  public my_class1='overlay';
  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;
  myDate:any;
  serverDate:any;


  hour:number;
  mins:any;
  secs:any;
  public my_class50:any='overlay';
  countries:any;
  selectedOpUnit:any;

  oldPwd:string;
  newPwd:string;
  confirmPwd:string;
  public my_class2='overlay';
  public my_class3='overlay';
  public my_class4='overlay';
  public my_class5='overlay';
  public my_class6='overlay';
  public my_class7='overlay';
  public my_class8='overlay';

  constructor(
    private clockService: ClockService,
    public authservice:AuthService,
    private router:Router,
    private flashMessagesService: FlashMessagesService,
    private idle: Idle,
    private keepalive: Keepalive,
    private envSpecificSvc: EnvironmentSpecificService
  ) {



          // Observable.interval(1000).subscribe(x => {
          //     //this.timeElapse();
          //     this.authservice.getServerTime().subscribe($time=>{
          //       // this.serverDate=$time.message;
          //       //console.log("Server time: ",this.serverDate);
          //     }
          //
          //   );
          // });

          this.countries = [
            { "id": 1, "name": "India" },
            { "id": 2, "name": "Australia" },
            { "id": 3, "name": "Brazil" }
           ];
           this.selectedOpUnit=   this.countries[0].id;

        this.location=localStorage.getItem('apmcSlocation');
        this.typeMLocation=localStorage.getItem('locationTypeM');
        this.userType=localStorage.getItem('userType');
        // sets an idle timeout of 5 seconds, for testing purposes.
      //   if(this.authservice.loggedIn()==true){
      //     //console.log("Hello user");
      //     idle.setIdle(60);
      //     // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
      //   //  idle.setTimeout(5);
      //     // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
      //     idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
      //     idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
      //     idle.onTimeout.subscribe(() => {
      //       this.idleState = 'Timed out!';
      //       this.timedOut = true;
      //       this.authservice.loggedOut().subscribe(
      //         ret=>{
      //           //let $ret=ret;
      //           //console.log("Logout response from server: ",ret);
      //           localStorage.clear();
      //           this.router.navigate(['/login', {sessionExpirate: 'true'}]);
      //         }
      //         , (err) => {
      //                 if (err === 'Unauthorized')
      //                 {
      //                   localStorage.clear();
      //                   this.router.navigateByUrl('/login');
      //             }
      //           }
      //       );
      //     });
      //     idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
      //     idle.onTimeoutWarning.subscribe((countdown) => this.idleState = 'You will time out in ' + countdown + ' seconds!');
      //
      //     // sets the ping interval to 15 seconds
      // //    keepalive.interval(150);
      //     keepalive.onPing.subscribe(() => this.lastPing = new Date());
      //     this.reset();
      //   }else{
      //     //console.log("loggedout");
      //   }

  }

  ngOnInit() {
    if((this.authservice.result) != undefined){

    }
    else{
      //console.log("Result in navbar is undefined");

    }


    this.clockService.getClock().subscribe(time => this.time = time);
    //this.location=(this.authservice.locationName);
    this.location=localStorage.getItem('location');
    this.typeMLocation=localStorage.getItem('locationTypeM');
    //console.log("Loc in Nav",this.location);
    this.userName=localStorage.getItem('userName');
    this.userPhoneNo=localStorage.getItem('userPhoneNo');
    this.userType=localStorage.getItem('userType');
    this.typeMLocation=localStorage.getItem('locationTypeM');
    this.emailId=localStorage.getItem('userEmailID');


  }



  reset() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }
  onLogoutClick(){

    this.logoutError=localStorage.getItem('logoutError');
    if(this.logoutError==0){
        //console.log("Connection error in logout");
        this.flashMessagesService.show('Connection error', { cssClass: 'alert-danger', timeout: 3000 });
      }
    this.authservice.loggedOut().subscribe(
      ret=>{
        //let $ret=ret;
        //console.log("Logout response from server: ",ret);
         localStorage.clear();
         this.router.navigate(['/login']);
      //  window.location.href = this.envSpecificSvc.envSpecific.eng+"login" ;
      }
      , (err) => {
            if (err === 'Unauthorized')
            {
              localStorage.clear();
              this.router.navigateByUrl('/login');
        }
      }
    );

  }

  getDark(){
      //console.log("Dark theme called");
      document.getElementById('chngTheme').setAttribute('href', 'assets/custom/css/theme_dark.css');
  }

  getLight(){
      //console.log("light theme called");
      document.getElementById('chngTheme').setAttribute('href', 'assets/custom/css/AdminLTE.min.css');
  }



  errorPopupShow(){
        this.my_class50='overlay1';
      }

  // validateform(){
  //   //console.log("Email id",this.userEmailID);
  //   this.pwd='';
  //   this.cpwd='';
  //   const sendPreference=
  //     {
  //       password:this.pwd,
  //       cPassWord:this.cpwd,
  //       contactNo:this.userPhoneNo,
  //       emailID:this.emailId,
  //       name:this.userName
  //     }
  //     if(this.pwd=='' && this.cpwd==''){
  //       this.flashMessagesService.show('fields can not be blank', { cssClass: 'alert-Success', timeout: 3000 });
  //     }
  //     else{
  //       this.authservice.getPasswordChange(sendPreference).subscribe(
  //           $ret=>{
  //             //console.log("Edit password reply from server",$ret);
  //             if($ret.status==1){
  //               this.flashMessagesService.show($ret.message, { cssClass: 'alert-Success', timeout: 3000 });
  //             }else{
  //               this.flashMessagesService.show('Your Preferences was not updated', { cssClass: 'alert-Success', timeout: 3000 });
  //             }
  //
  //           }
  //           , (err) => {
  //           if (err === 'Unauthorized')
  //           {
  //             localStorage.clear();
  //             this.router.navigateByUrl('/login');
  //       }
  //     }
  //         )
  //     }
  // }

  pop_show(){
    this.my_class1='overlay1';
  }
  pop_hide(){
    this.selectedOpUnit=   this.countries[0].id;
    this.my_class1='overlay';

  }
  // Selecting operating unit
onSelectOperatingUnit(selectedId){
  console.log("selected id",selectedId);

}

// Validate password
loginId:string;saveMsg:string;errorMsg:string;
savePrefernces(){
  if(this.oldPwd != "" && this.oldPwd != undefined){
    this.loginId = localStorage.getItem("loginID");
    var postJson={"loginID": this.loginId , "password": this.oldPwd};
    this.authservice.validatePassword(postJson).subscribe(
        $ret=>{
          console.log("Val password reply from server",$ret);
          if($ret.status==1){
            if(this.newPwd != "" && this.newPwd != undefined){
              if(this.confirmPwd != "" && this.confirmPwd != undefined){
                if(this.newPwd == this.confirmPwd){
                  var saveJson={"password": this.newPwd,"cPassWord": this.confirmPwd}
                  this.authservice.getPasswordChange(saveJson).subscribe(
                      $ret=>{
                        console.log("pwd change res",$ret)
                        if($ret.status==1){
                          this.saveMsg = $ret.message;
                          this.my_class5 = 'overlay1';
                          setTimeout(()=>{
                            this.my_class5 = 'overlay';
                            this.my_class1='overlay';
                            this.onResetFields();
                          },2000);

                        }else{
                          this.errorMsg = $ret.message;
                          this.my_class6 = 'overlay1';
                          setTimeout(()=>{
                            this.my_class6 = 'overlay';
                          },2000);
                        }
                      }
                      , (err) => {
                      if (err === 'Unauthorized')
                      {
                        localStorage.clear();
                        this.router.navigateByUrl('/login');
                  }
                }
                    )
                }
                else{
                     this.newPwd ="";
                     this.confirmPwd ="";
                     this.my_class4 = 'overlay1';
                     setTimeout(()=>{
                       this.my_class4 = 'overlay';
                     },2000);            }
              }
              else{
                this.my_class8 = 'overlay1';
                setTimeout(()=>{
                  this.my_class8 = 'overlay';
                },2000);
              }
            }
            else{
              this.my_class7 = 'overlay1';
              setTimeout(()=>{
                this.my_class7 = 'overlay';
              },2000);
            }
          }
          else{
            this.my_class2 = 'overlay1';
            setTimeout(()=>{
              this.my_class2 = 'overlay';
            },2000);
          }
        }
        , (err) => {
        if (err === 'Unauthorized')
        {
          localStorage.clear();
          this.router.navigateByUrl('/login');
    }
  }
      )
  }
  else{
    this.my_class3 = 'overlay1';
    setTimeout(()=>{
      this.my_class3 = 'overlay';
    },2000);  }
}

onResetFields(){
  this.oldPwd = "";
  this.newPwd = "";
  this.confirmPwd = "";
  this.saveMsg = "";
  this.errorMsg = "";
}
}
